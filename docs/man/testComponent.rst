=============
testComponent
=============

----------------------------------------------------
Example of a component for DIET using the LogCentral
----------------------------------------------------

:Authors: haikel.guemar@sysfera.com, kevin.coulomb@sysfera.com
:Date:   2011-05-16
:Copyright: DIET developers
:License: GPLv3
:Version: 0.1
:Manual section: 1

NAME
====

testComponent - Example of a component that connects to the LogCentral and 
publish some messages. It is used as an example for the DIET program. 


SYNOPSYS
========

  testComponent

DESCRIPTION
===========

testComponent connects to the LogCentral, publishes some messages and
then disconnects.

Before starting the LogCentral, you must:

* launch a CORBA Naming Service.

* launch log forwarders *if needed*

* launch LogCentral service

* launch a tool to visualize the message sent

* Launch the testComponent



OPTIONS
=======



EXAMPLE
=======

* Launching the LogCentral

::

  LogCentral -config ./LogCentral.cfg

* Launching the component

::

  testComponent


RATIONALE
=========

The testComponent uses CORBA as its communication layer. While it's a flexible and
robust middleware, its deployement on heterogeneous networks still is delicate 
and may require using ssh tunnels.

LICENSE AND COPYRIGHT
=====================
    
Copyright
---------    
(C)2011, GRAAL, INRIA Rhone-Alpes, 46 allee d'Italie, 69364 Lyon
cedex 07, France all right reserved <diet-dev@ens-lyon.fr>

License
-------
This program is free software: you can redistribute it and/or mod‐
ify it under the terms of the GNU General Public License as pub‐
lished by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. This program is
distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Pub- lic Li‐
cense for more details. You should have received a copy of the GNU
General Public License along with this program. If not, see
<http://www.gnu.org/licenses/>.

AUTHORS
=======
GRAAL
INRIA Rhone-Alpes
46 allee d'Italie 69364 Lyon cedex 07, FRANCE
Email: <diet-dev@ens-lyon.fr>
WWW: http://graal.ens-lyon.fr/DIET

SEE ALSO
========
omniNames(1), logForwarder(1), LogCentral(1), DIETTestTool(1)

BUGS
====
