=============
DIETTestTool
=============

-----------------------------------------------
Example of a tool for DIET using the LogCentral
-----------------------------------------------

:Authors: haikel.guemar@sysfera.com, kevin.coulomb@sysfera.com
:Date:   2011-05-16
:Copyright: DIET developers
:License: GPLv3
:Version: 0.1
:Manual section: 1

NAME
====

DIETTestTool - Example of a tool that connects to the LogCentral without
any filter and that display all the messages published. It is used as an example
for the DIET program. All received published messages are also stored 
in a log file.


SYNOPSYS
========

  DIETTestTool [options] ...

DESCRIPTION
===========

DIETTestTool connects to the LogCentral and get the messages (by default all,
a file containing filters may be passed as an argument). The tool both display
and save the messages in a log file. This log file may be given or will be 
created with a fix name "DIETLogTool.log".

Before starting the LogCentral, you must:

* launch a CORBA Naming Service.

* launch log forwarders *if needed*

* launch LogCentral service

* Launch the DIETTestTool

* Use a component to publish messages

OPTIONS
=======

* The first argument is the destination output file that will contain the displayed logs.

* The second one is a filter to use in the tool

[Remark: There is no option letter, the order is fixed]




EXAMPLE
=======

* Launching the LogCentral

::

  LogCentral -config ./LogCentral.cfg

* Launching the tool 

::

  DIETTestTool

* Launching the tool to store the logs in the toto.log file

::

  DIETTestTool toto.log

* Launching the tool to store the logs in the toto.log file with the filter in the filter.txt file

::

  DIETTestTool toto.log filter.txt

RATIONALE
=========

The LogTool uses CORBA as its communication layer. While it's a flexible and
robust middleware, its deployement on heterogeneous networks still is delicate 
and may require using ssh tunnels.

LICENSE AND COPYRIGHT
=====================
    
Copyright
---------    
(C)2011, GRAAL, INRIA Rhone-Alpes, 46 allee d'Italie, 69364 Lyon
cedex 07, France all right reserved <diet-dev@ens-lyon.fr>

License
-------
This program is free software: you can redistribute it and/or mod‐
ify it under the terms of the GNU General Public License as pub‐
lished by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. This program is
distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Pub- lic Li‐
cense for more details. You should have received a copy of the GNU
General Public License along with this program. If not, see
<http://www.gnu.org/licenses/>.

AUTHORS
=======
GRAAL
INRIA Rhone-Alpes
46 allee d'Italie 69364 Lyon cedex 07, FRANCE
Email: <diet-dev@ens-lyon.fr>
WWW: http://graal.ens-lyon.fr/DIET

SEE ALSO
========
omniNames(1), logForwarder(1), LogCentral(1), testComponent(1)

BUGS
====
