=============
LogCentral
=============

----------------------------------------------------
Central logging service for distributed applications
----------------------------------------------------

:Authors: haikel.guemar@sysfera.com, kevin.coulomb@sysfera.com
:Date:   2011-05-11
:Copyright: DIET developers
:License: GPLv3
:Version: 0.1
:Manual section: 1

NAME
====

LogCentral - service using the publisher/subscriber model, where
components publish tagged messages and tools get back appropriate messages .


SYNOPSYS
========

  LogCentral [options] ...

DESCRIPTION
===========

LogCentral gathers and publishes the messages to the subscribers. 
Messages are tagged, the LogCentral service only sends suscribers 
filtered messages.

Before starting the LogCentral, you must:

* launch a CORBA Naming Service.

* launch log forwarders *if needed*

* launch LogCentral service

[Remark: LogCentral must be launched before the log tools/components]

OPTIONS
=======

**-config** [name]
  The configuration file for the LogCentral service.
  First, LogCentral will use the file provided by command-line, then read environment
  variable $LOGCENTRAL_CONFIG and in last resort, it will look for a file named "config.cfg" 
  in current directory.


CONFIGURATION FILE
==================

You can pass a configuration file to LogCentral using command line
options through the -config option. Configuration file lists several classes of 
tags describing the messages. A minimal configuration will look like this:

[General]

[DynamicTagList]
[StaticTagList]
[UniqueTagList]
[VolatileTagList]


The general tag accept the following parameters:
* 'port=xxx', specifies the port to use

* 'MinAge=xxx', defines the minimum period of time that messages will be stored in the LogCentral

* 'DynamicStartSuffix=START', expands tags with START. See DynamicTagList.

* 'DynamicStopSuffix=STOP', expands tags with STOP. See DynamicTagList.

The other four categories configure the state manager of the LogCentral. It is
important to know that all tags will be expanded with two suffixes to generate 
pairs of tags.

* VolatileTagList, should contain all the monitored tags. Not essential but it may be requested by some tool.

* UniqueTagList, should contain tags that will overwrite the previous messages of this tag



EXAMPLE
=======

* Launching the LogCentral

::

  LogCentral -config ./LogCentral.cfg


RATIONALE
=========

The LogCentral uses CORBA as its communication layer. While it's a flexible and
robust middleware, its deployement on heterogeneous networks still is delicate 
and may require using ssh tunnels.

LICENSE AND COPYRIGHT
=====================
    
Copyright
---------    
(C)2011, GRAAL, INRIA Rhone-Alpes, 46 allee d'Italie, 69364 Lyon
cedex 07, France all right reserved <diet-dev@ens-lyon.fr>

License
-------
This program is free software: you can redistribute it and/or mod‐
ify it under the terms of the GNU General Public License as pub‐
lished by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. This program is
distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Pub- lic Li‐
cense for more details. You should have received a copy of the GNU
General Public License along with this program. If not, see
<http://www.gnu.org/licenses/>.

AUTHORS
=======
GRAAL
INRIA Rhone-Alpes
46 allee d'Italie 69364 Lyon cedex 07, FRANCE
Email: <diet-dev@ens-lyon.fr>
WWW: http://graal.ens-lyon.fr/DIET

SEE ALSO
========
omniNames(1), logForwarder(1)

BUGS
====
