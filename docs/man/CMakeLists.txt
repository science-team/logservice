##############################################################################
# man pages generation
# we need rst2man for the generation
#
##############################################################################
find_program(RST2MAN_BIN rst2man)
if(RST2MAN_BIN)
  # create man output directory
  file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/man1")

  add_custom_target(man ALL)

  file(GLOB MAN_PAGES_SRCS 
    RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "*.rst")
  
  foreach(SRC ${MAN_PAGES_SRCS})
    string(REGEX REPLACE "(.*).rst$" "\\1" BASENAME ${SRC})
    add_custom_target("man-${BASENAME}"
      ${RST2MAN_BIN} "${CMAKE_CURRENT_SOURCE_DIR}/${SRC}" "man1/${BASENAME}.1")
    add_dependencies(man "man-${BASENAME}") 
  endforeach()
  install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/man1"
  DESTINATION "${MAN_INSTALL_DIR}")
else()
  message(WARNING " Man pages will not be built due to missing requirements.")
  message(WARNING " Please install rst2man (usually included in python-docutils package)")
  install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    DESTINATION ${MAN_INSTALL_DIR}
    FILES_MATCHING PATTERN "*.1")
endif()

