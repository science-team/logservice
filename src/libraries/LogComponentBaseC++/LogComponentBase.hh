/****************************************************************************/
/* LogComponentBase header class                                            */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.5  2011/02/04 15:53:55  bdepardo
 * Use a const char* name in constructor, then strdup the name.
 * Then use free instead of delete on this->name
 *
 * Revision 1.4  2010/12/03 12:40:26  kcoulomb
 * MAJ log to use forwarders
 *
 * Revision 1.3  2010/11/10 02:27:44  kcoulomb
 * Update the log to use the forwarder.
 * Programm run without launching forwarders but fails with forwarder.
 *
 * Revision 1.2  2007/08/31 16:41:17  bdepardo
 * When trying to add a new component, we check if the name of the component exists and if the component is reachable
 * - it the name already exists:
 *    - if the component is reachable, then we do not connect the new component
 *    - else we consider that the component is lost, and we delete the old component ant add the new one
 * - else add the component
 *
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#ifndef _LOGCOMPONENTBASE_HH_
#define _LOGCOMPONENTBASE_HH_

#include <omnithread.h>

#include "LogTypes.hh"
#include "ComponentConfigurator_impl.hh"
#include "PingThread.hh"
#include "FlushBufferThread.hh"
#include "LogComponent.hh"

#define PINGTHREAD_SLEEP_TIME_SEC          1
#define PINGTHREAD_SLEEP_TIME_NSEC         0
#define PINGTHREAD_SYNCHRO_FREQUENCY       60
#define FLUSHBUFFERTHREAD_SLEEP_TIME_SEC   0
#define FLUSHBUFFERTHREAD_SLEEP_TIME_NSEC  50000000

class LogComponentBase;

class LogComponentBase {
public:
  LogComponentBase(bool* success, int argc, char** argv,
                   unsigned int tracelevel, const char* name,
                   unsigned int port = 0);

  ~LogComponentBase();

  void
  setName(const char* name);

  char*
  getName();

  char*
  getHostname();

  short
  connect(const char* message);

  short
  disconnect(const char* message);

  void
  sendMsg(const char* tag, const char* msg);

  bool
  isLog(const char* tagname);

  LogCentralComponent_var LCCref;
  ComponentConfigurator_var CCref;
  log_msg_buf_t buffer;
  tag_list_t currentTagList;

private:
  log_time_t
  getLocalTime();

  char* name;
  char* hostname;
  ComponentConfigurator_impl* CCimpl;
  PingThread* pingThread;
  FlushBufferThread* flushBufferThread;
};

#endif

