/****************************************************************************/
/* LogToolBase header class                                                 */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#ifndef _LOGTOOLBASE_HH_
#define _LOGTOOLBASE_HH_

#include "LogTypes.hh"
#include "LogTool.hh"

class LogToolBase;
class ToolMsgReceiver_impl: public POA_ToolMsgReceiver,
                            public PortableServer::RefCountServantBase {
public:
  explicit ToolMsgReceiver_impl(LogToolBase* LTB);

  ~ToolMsgReceiver_impl();

  void
  sendMsg(const log_msg_buf_t& msgBuf);

private:
  LogToolBase* LTB;
};


class LogToolBase {
public:
  LogToolBase(bool* succes, int argc, char** argv,
              unsigned int tracelevel, unsigned int port = 0);

  virtual
  ~LogToolBase();

  void
  setName(const char* name);

  char*
  getName();

  short
  connect();

  short
  disconnect();

  short
  addFilter(const filter_t& filter);

  short
  removeFilter(const char* filterName);

  short
  flushAllFilters();

  tag_list_t*
  getDefinedTags();

  component_list_t*
  getDefinedComponents();

  virtual
  void
  sendMsg(const log_msg_t& msg) = 0;

private:
  LogCentralTool_var LCTref;
  ToolMsgReceiver_impl* TMRimpl;
  ToolMsgReceiver_var TMRref;
  char* name;
};

#endif

