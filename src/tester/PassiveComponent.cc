/****************************************************************************/
/* Encapsulates Component functionality for testing                         */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.2  2008/07/17 01:03:12  rbolze
 * make some change to avoid gcc warning
 *
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#include "PassiveComponent.hh"
#include <cstdio>
#include <cstring>
#include "LocalTime.hh"


/**
 * Some little helpers
 */
bool
isIncludedTagList(tag_list_t* t1, tag_list_t* t2) {
  bool found;
  for (unsigned int i = 0; i < t1->length(); i++) {
    // find a corresponding tag for each tag in list 1
    found = false;
    for (unsigned int j = 0; j < t2->length(); j++) {
      if (strcmp((*t1)[i], (*t2)[j]) == 0) {
        found = true;
      }
    }
    if (!found) {
      return false;
    }
  }
  return true;
}

bool
isEqualTagList(tag_list_t* t1, tag_list_t* t2) {
  if (!isIncludedTagList(t1, t2)) {
    return false;
  }
  if (!isIncludedTagList(t2, t1)) {
    return false;
  }
  return true;
}

void
printTagList(const char* s, tag_list_t* t) {
  printf("%s", s);
  for (unsigned int i = 0; i < t->length(); i++) {
    printf("<%s>", (char*)(*t)[i]);
  }
  printf("\n");
}

PassiveComponent::PassiveComponent() {
  myCC = new ComponentConfigurator_impl(&currentConfig);
}

PassiveComponent::~PassiveComponent() {
}

void
PassiveComponent::activate(PortableServer::POA_var poa,
                           LogCentralComponent_var aLCCref) {
  myCCid = poa->activate_object(myCC);
  aCCref = myCC->_this();
  myCC->_remove_ref();
  myCC = NULL;

  this->aLCCref = aLCCref;
}

void
PassiveComponent::checkConnect(const char* componentName,
                               const char* hostName,
                               const char* msg,
                               int retValExpected,
                               tag_list_t expectedConfig,
                               bool* success) {
  int retVal;
  bool retCorrect = true;
  bool icfgCorrect = true;
  tag_list_t initialConfig;
  this->componentName = CORBA::string_dup(componentName);

  retVal = aLCCref->connectComponent(this->componentName,
                                     hostName,
                                     msg,
                                     aCCref,
                                     getLocalTime(),
                                     initialConfig);

  if (retVal != retValExpected) {
    retCorrect = false;
  }
  if (!isEqualTagList(&expectedConfig, &initialConfig)) {
    icfgCorrect = false;
  }

  if (!(retCorrect && icfgCorrect)) {
    printf("ERROR: ConnectComponent('%s') did not meet expectations\n",
           (char*)this->componentName);
    if (!retCorrect) {
      printf("  Errorcode is not correct\n");
      printf("    returned value: %d\n", retVal);
      printf("    expected value: %d\n", retValExpected);
    }
    if (!icfgCorrect) {
      printf("  Initial Config is not correct\n");
      printTagList("    returned config: ", &initialConfig);
      printTagList("    expected config:    ", &expectedConfig);
    }
    *success = false;
  }

  // dont forget to set initial config if everything is correct
  currentConfig = initialConfig;
  this->expectedConfig = expectedConfig;
}

void
PassiveComponent::checkDisconnect(const char* msg, bool* success) {
  checkDisconnect(this->componentName, msg, LS_OK, success);
}

void
PassiveComponent::checkDisconnect(const char* componentName, const char* msg,
                                  int retValExpected, bool* success) {
  int retVal;
  retVal = aLCCref->disconnectComponent(componentName, msg);
  if (retVal != retValExpected) {
    printf("ERROR: DisconnectComponent('%s') did not meet expectations\n",
           componentName);
    printf("  Errorcode is not correct\n");
    printf("    returned value: %d\n", retVal);
    printf("    expected value: %d\n", retValExpected);
    *success = false;
  }
}

void
PassiveComponent::addMessage(const char* tag, const char* msg) {
  log_msg_t logMsg;
  logMsg.componentName = CORBA::string_dup(componentName);
  logMsg.tag = CORBA::string_dup(tag);
  logMsg.msg = CORBA::string_dup(msg);
  logMsg.time = getLocalTime();
  addMessage(logMsg);
}

void
PassiveComponent::addMessage(const log_msg_t logMsg) {
  CORBA::Long len;
  len = logMsgBuf.length();
  logMsgBuf.length(len+1);
  logMsgBuf[len] = logMsg;
}

void
PassiveComponent::sendMsgBuf() {
  aLCCref->sendBuffer(logMsgBuf);
  logMsgBuf.length(0);
}

void
PassiveComponent::setExpectedConfig(tag_list_t expectedConfig) {
  this->expectedConfig = expectedConfig;
}

void
PassiveComponent::addExpectedConfig(const char* tag) {
  CORBA::ULong i;
  i = expectedConfig.length();
  expectedConfig.length(i + 1);
  expectedConfig[i] = CORBA::string_dup(tag);
}

void
PassiveComponent::checkConfig(bool* success) {
  if (!isEqualTagList(&currentConfig, &expectedConfig)) {
    printf("ERROR: component '%s' does not meet expectations\n",
           (char*)componentName);
    printf("  TagFilter is not correct\n");
    printTagList("    current config:  ", &currentConfig);
    printTagList("    expected config: ", &expectedConfig);
    *success = false;
  }
}

