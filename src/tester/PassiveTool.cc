/****************************************************************************/
/* Encapsulates Tool functionality for testing                              */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.3  2011/02/04 15:37:52  bdepardo
 * Reduce variable scope
 *
 * Revision 1.2  2008/07/17 01:03:12  rbolze
 * make some change to avoid gcc warning
 *
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#include "PassiveTool.hh"
#include <stdio.h>

/**
 * some little helpers
 */
void
printMsg(const char* s, log_msg_t msg) {
  printf("%s", s);
  printf("msg from '%s': %s (%s)\n", (char*)msg.componentName,
         (char*)msg.msg, (char*)msg.tag);
}

void
printMsgBuf(const char* s, log_msg_buf_t msgBuf) {
  if (msgBuf.length() == 0) {
    printf("%s<empty>\n", s);
  }
  for (CORBA::ULong i = 0; i < msgBuf.length(); i++) {
    printMsg(s, msgBuf[i]);
  }
}


PassiveTool::PassiveTool() {
  myTMR = new ToolMsgReceiver_impl(&receivedMsgBuf);
}

PassiveTool::~PassiveTool() {
}

void
PassiveTool::activate(PortableServer::POA_var poa,
                      LogCentralTool_var aLCTref) {
  myTMRid = poa->activate_object(myTMR);
  aTMRref = myTMR->_this();
  myTMR->_remove_ref();
  myTMR = NULL;

  this->aLCTref = aLCTref;
}

void
PassiveTool::checkConnect(const char* toolName, int retValExpected,
                          bool* success) {
  int retVal;
  this->toolName = CORBA::string_dup(toolName);
  retVal = aLCTref->connectTool(this->toolName, aTMRref);
  if (retVal != retValExpected) {
    printf("ERROR: ConnectTool('%s') did not meet expectations\n",
           (char*)this->toolName);
    printf("  Errorcode is not correct\n");
    printf("    returned value: %d\n", retVal);
    printf("    expected value: %d\n", retValExpected);
    *success = false;
  }
}

void
PassiveTool::checkDisconnect(bool* success) {
  checkDisconnect(toolName, LS_OK, success);
}

void
PassiveTool::checkDisconnect(const char* toolName, int retValExpected,
                             bool* success) {
  int retVal;
  retVal = aLCTref->disconnectTool(toolName);
  if (retVal != retValExpected) {
    printf("ERROR: DisconnectTool('%s') did not meet expectations\n", toolName);
    printf("  Errorcode is not correct\n");
    printf("    returned value: %d\n", retVal);
    printf("    expected value: %d\n", retValExpected);
    *success = false;
  }
}

void
PassiveTool::checkAddFilter(filter_t filter, bool* success) {
  checkAddFilter(toolName, filter, LS_OK, success);
}

void
PassiveTool::checkAddFilter(const char* toolName, filter_t filter,
                            int retValExpected, bool* success) {
  int retVal;
  retVal = aLCTref->addFilter(toolName, filter);
  if (retVal != retValExpected) {
    printf("ERROR: addFilter '%s' for tool '%s' did not meet expectations\n",
           (char*)filter.filterName, toolName);
    printf("  Errorcode is not correct\n");
    printf("    returned value: %d\n", retVal);
    printf("    expected value: %d\n", retValExpected);
    *success = false;
  }
}

void
PassiveTool::checkRemoveFilter(const char* filterName, bool* success) {
  checkRemoveFilter(toolName, filterName, LS_OK, success);
}

void
PassiveTool::checkRemoveFilter(const char* toolName, const char* filterName,
                               int retValExpected, bool* success) {
  int retVal;
  retVal = aLCTref->removeFilter(toolName, filterName);
  if (retVal != retValExpected) {
    printf("ERROR: removeFilter '%s' for tool '%s' did not meet expectations\n",
           filterName, toolName);
    printf("  Errorcode is not correct\n");
    printf("    returned value: %d\n", retVal);
    printf("    expected value: %d\n", retValExpected);
    *success = false;
  }
}

void
PassiveTool::checkFlushAllFilters(bool* success) {
  checkFlushAllFilters(toolName, LS_OK, success);
}

void
PassiveTool::checkFlushAllFilters(const char* toolName, int retValExpected,
                                  bool* success) {
  int retVal;
  retVal = aLCTref->flushAllFilters(toolName);
  if (retVal != retValExpected) {
    printf("ERROR: flushAllFilters for tool '%s' did not meet expectations\n",
           toolName);
    printf("  Errorcode is not correct\n");
    printf("    returned value: %d\n", retVal);
    printf("    expected value: %d\n", retValExpected);
    *success = false;
  }
}

void
PassiveTool::setExpectedMsgBuf(log_msg_buf_t expectedMsgBuf) {
  this->expectedMsgBuf = expectedMsgBuf;
}

void
PassiveTool::addExpectedMsg(const char* componentName, const char* msg,
                            const char* tag) {
  log_msg_t logMsg;
  logMsg.componentName = CORBA::string_dup(componentName);
  logMsg.msg = CORBA::string_dup(msg);
  logMsg.tag = CORBA::string_dup(tag);
  addExpectedMsg(logMsg);
}

void
PassiveTool::addExpectedMsg(log_msg_t msg) {
  CORBA::ULong i;
  i = expectedMsgBuf.length();
  expectedMsgBuf.length(i+1);
  expectedMsgBuf[i] = msg;
}


void
PassiveTool::checkMsgBuf(bool* success) {
  bool bufCorrect;

  bufCorrect = true;

  if (receivedMsgBuf.length() != expectedMsgBuf.length()) {
    bufCorrect = false;
  } else {
    // buffers have equal length, we can compare each element
    bool msgCorrect;
    for (CORBA::ULong i = 0; i < receivedMsgBuf.length(); i++) {
      msgCorrect = true;
      if (strcmp(receivedMsgBuf[i].componentName,
                 expectedMsgBuf[i].componentName) != 0) {
        msgCorrect = false;
      }
      if (strcmp(receivedMsgBuf[i].tag, expectedMsgBuf[i].tag) != 0) {
        msgCorrect = false;
      }
      if (strcmp(receivedMsgBuf[i].msg, expectedMsgBuf[i].msg) != 0) {
        msgCorrect = false;
      }
      // dont check for the time
      if (msgCorrect == false) {
        bufCorrect = false;
        break;
      }
    }
  }
  if (!bufCorrect) {
    printf("ERROR: tool '%s' does not meet expectations\n", (char*)toolName);
    printf("  MsgBuffer is not correct\n");
    printf("    received msgBuf:\n");
    printMsgBuf("      ", receivedMsgBuf);
    printf("    expected msgBuf:\n");
    printMsgBuf("      ", expectedMsgBuf);
    *success = false;
  }

  // reset the buffers
  expectedMsgBuf.length(0);
  receivedMsgBuf.length(0);
}

