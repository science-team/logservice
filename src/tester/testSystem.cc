/****************************************************************************/
/* Tests the LogCentral internal functionality                              */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#include <cstdio>
#include <cstring>
#include <iostream>
#include <unistd.h>


// "extern" servants
#include "PassiveTool.hh"
#include "PassiveComponent.hh"

// idl
#include "LogTypes.hh"
#include "LogTool.hh"
#include "LogComponent.hh"

// monitor components
#include "LogCentralTool_impl.hh"
#include "LogCentralComponent_impl.hh"
#include "ToolList.hh"
#include "ComponentList.hh"
#include "SimpleFilterManager.hh"
#include "FilterManagerInterface.hh"
#include "StateManager.hh"
#include "Options.hh"
#include "ReadConfig.hh"
#include "LocalTime.hh"
#include "TimeBuffer.hh"

// threads
#include "SendThread.hh"
#include "CoreThread.hh"



#define COMP_COUNT 5
#define TOOL_COUNT 5
#define TEST_COUNT 10


int
main(int argc, char** argv) {
  printf("\n");
  printf("LogCentral Test\n");
  printf("===============================================================\n");
  printf("This programm tests the complete LogCentral system by creating \n");
  printf("and connecting all components properly. It then launches       \n");
  printf("certain tests and assures the results match the expectations.  \n");
  printf("\n");

  /**
   * Systemcomponents
   */
  ToolList* toolList;
  ComponentList* componentList;
  ReadConfig* readConfig;
  tag_list_t* stateTags;
  tag_list_t* allTags;

  StateManager* stateManager;
  SimpleFilterManager* simpleFilterManager;
  TimeBuffer* timeBuffer;

  LogCentralTool_impl* myLCT;
  PortableServer::ObjectId_var myLCTid;
  LogCentralTool_var aLCTref;

  LogCentralComponent_impl* myLCC;
  PortableServer::ObjectId_var myLCCid;
  LogCentralComponent_var aLCCref;

  SendThread* sendThread;
  CoreThread* coreThread;

  // build system
  bool success;

  toolList = new ToolList;
  componentList = new ComponentList;

  readConfig = new ReadConfig("config.cfg", &success);
  readConfig->parse();
  stateTags = readConfig->getStateTags();
  allTags = readConfig->getAllTags();

  stateManager = new StateManager(readConfig, &success);
  simpleFilterManager =
    new SimpleFilterManager(toolList, componentList, stateTags);
  timeBuffer = new TimeBuffer();

  sendThread = new SendThread(toolList);
  coreThread = new CoreThread(timeBuffer, stateManager,
                              simpleFilterManager, toolList);

  myLCT = new LogCentralTool_impl(toolList, componentList,
                                  simpleFilterManager, stateManager, allTags);
  myLCC =
    new LogCentralComponent_impl(componentList, simpleFilterManager,
                                 timeBuffer);

  delete allTags;
  delete stateTags;
  delete readConfig;

  /**
   * Testcomponents
   */
  PassiveTool testTool[TOOL_COUNT];
  PassiveComponent testComp[COMP_COUNT];


  /**
   * Start ORB and initialize Servants
   */
  CORBA::ORB_ptr orb = CORBA::ORB_init(argc , argv, "omniORB4");
  CORBA::Object_var obj = orb->resolve_initial_references("RootPOA");
  PortableServer::POA_var poa = PortableServer::POA::_narrow(obj);


  // LogCentralTool_impl
  myLCTid = poa->activate_object(myLCT);
  aLCTref = myLCT->_this();
  myLCT->_remove_ref();
  myLCT = NULL;

  // LogCentralComponent_impl
  myLCCid = poa->activate_object(myLCC);
  aLCCref = myLCC->_this();
  myLCC->_remove_ref();
  myLCC = NULL;

  for (int i = 0; i < TOOL_COUNT; i++) {
    testTool[i].activate(poa, aLCTref);
  }
  for (int i = 0; i < COMP_COUNT; i++) {
    testComp[i].activate(poa, aLCCref);
  }

  /**
   * Start off
   */

  // activate manager
  PortableServer::POAManager_var pman = poa->the_POAManager();
  pman->activate();

  // start our threads
  coreThread->startThread();
  sendThread->startThread();


  /**************************************
   * Do Tests here...
   **************************************/

  // create some basic tagLists and configs for all tests
  char strBuf[50];
  char strBuf2[50];

  tag_list_t initialConfig;
  initialConfig.length(5);
  initialConfig[0] = CORBA::string_dup("DYNAMIC_START");
  initialConfig[1] = CORBA::string_dup("DYNAMIC_STOP");
  initialConfig[2] = CORBA::string_dup("STATIC");
  initialConfig[3] = CORBA::string_dup("UNIQUE1");
  initialConfig[4] = CORBA::string_dup("UNIQUE2");

  tag_list_t allConfig;
  allConfig.length(1);
  allConfig[0] = CORBA::string_dup("*");

  filter_t filterAll;
  filterAll.filterName = CORBA::string_dup("filterAll");
  filterAll.tagList.length(1);
  filterAll.tagList[0] = CORBA::string_dup("*");
  filterAll.componentList.length(1);
  filterAll.componentList[0] = CORBA::string_dup("*");

  filter_t filterComponent[COMP_COUNT];
  for (int i = 0; i < COMP_COUNT; i++) {
    snprintf(strBuf, 50, "filterComponent %d", i);
    filterComponent[i].filterName = CORBA::string_dup(strBuf);
    filterComponent[i].tagList.length(1);
    filterComponent[i].tagList[0] = CORBA::string_dup("*");
    snprintf(strBuf, 50, "component %d", i);
    filterComponent[i].componentList.length(1);
    filterComponent[i].componentList[0] = CORBA::string_dup(strBuf);
  }

  // we will ignore the [0], as we have no VOLATILE0 Tag
  filter_t filterVolatile[4];
  for (int i = 0; i < 4; i++) {
    snprintf(strBuf, 50, "filterVolatile %d", i);
    filterVolatile[i].filterName = CORBA::string_dup(strBuf);
    snprintf(strBuf, 50, "VOLATILE%d", i);
    filterVolatile[i].tagList.length(1);
    filterVolatile[i].tagList[0] = CORBA::string_dup(strBuf);
    filterVolatile[i].componentList.length(1);
    filterVolatile[i].componentList[0] = CORBA::string_dup("*");
  }

  // filter for comp: comp0,1 tags: volatile1,2
  filter_t filterC01T12;
  filterC01T12.filterName = CORBA::string_dup("filterC01T01");
  filterC01T12.tagList.length(2);
  filterC01T12.tagList[0] = CORBA::string_dup("VOLATILE1");
  filterC01T12.tagList[1] = CORBA::string_dup("VOLATILE2");
  filterC01T12.componentList.length(2);
  filterC01T12.componentList[0] = CORBA::string_dup("component 0");
  filterC01T12.componentList[1] = CORBA::string_dup("component 1");

  // filter for comp: comp1,2 tags: volatile2,3
  filter_t filterC12T23;
  filterC12T23.filterName = CORBA::string_dup("filterC12T23");
  filterC12T23.tagList.length(2);
  filterC12T23.tagList[0] = CORBA::string_dup("VOLATILE2");
  filterC12T23.tagList[1] = CORBA::string_dup("VOLATILE3");
  filterC12T23.componentList.length(2);
  filterC12T23.componentList[0] = CORBA::string_dup("component 1");
  filterC12T23.componentList[1] = CORBA::string_dup("component 2");

  // filter for comp: comp3,4 tags: volatile1,2
  filter_t filterC34T12;
  filterC34T12.filterName = CORBA::string_dup("filterC34T12");
  filterC34T12.tagList.length(2);
  filterC34T12.tagList[0] = CORBA::string_dup("VOLATILE1");
  filterC34T12.tagList[1] = CORBA::string_dup("VOLATILE2");
  filterC34T12.componentList.length(2);
  filterC34T12.componentList[0] = CORBA::string_dup("component 3");
  filterC34T12.componentList[1] = CORBA::string_dup("component 4");

  // set of useless Tags (stateTags)
  filter_t filterUseless;
  filterUseless.filterName = CORBA::string_dup("filterUseless");
  filterUseless.tagList.length(4);
  filterUseless.tagList[0] = CORBA::string_dup("STATIC");
  filterUseless.tagList[1] = CORBA::string_dup("UNIQUE1");
  filterUseless.tagList[2] = CORBA::string_dup("IN");
  filterUseless.tagList[3] = CORBA::string_dup("OUT");
  filterUseless.componentList.length(3);
  filterUseless.componentList[0] = CORBA::string_dup("component 0");
  filterUseless.componentList[1] = CORBA::string_dup("component 2");
  filterUseless.componentList[2] = CORBA::string_dup("component 3");

  // complete Filterset for comp0-4, enriched with useless tags (stateTags);
  filter_t filterComplete1;
  filterComplete1.filterName = CORBA::string_dup("filterComplete1");
  filterComplete1.tagList.length(4);
  filterComplete1.tagList[0] = CORBA::string_dup("VOLATILE1");
  filterComplete1.tagList[1] = CORBA::string_dup("VOLATILE3");
  filterComplete1.tagList[2] = CORBA::string_dup("DYNAMIC_START");
  filterComplete1.tagList[3] = CORBA::string_dup("DYNAMIC_STOP");
  filterComplete1.componentList.length(2);
  filterComplete1.componentList[0] = CORBA::string_dup("component 0");
  filterComplete1.componentList[1] = CORBA::string_dup("component 4");

  filter_t filterComplete2;
  filterComplete2 = filterComponent[1];
  filterComplete2.filterName = CORBA::string_dup("filterComplete2");
  filterComplete2.componentList.length(2);
  filterComplete2.componentList[1] = CORBA::string_dup("component 3");

  filter_t filterComplete3;
  filterComplete3.filterName = CORBA::string_dup("filterComplete3");
  filterComplete3.tagList.length(5);
  filterComplete3.tagList[0] = CORBA::string_dup("VOLATILE1");
  filterComplete3.tagList[1] = CORBA::string_dup("VOLATILE2");
  filterComplete3.tagList[2] = CORBA::string_dup("VOLATILE3");
  filterComplete3.tagList[3] = CORBA::string_dup("UNIQUE1");
  filterComplete3.tagList[4] = CORBA::string_dup("UNKNOWN");
  filterComplete3.componentList.length(1);
  filterComplete3.componentList[0] = CORBA::string_dup("component 2");

  filter_t filterComplete4;
  filterComplete4.filterName = CORBA::string_dup("filterComplete4");
  filterComplete4.tagList.length(3);
  filterComplete4.tagList[0] = CORBA::string_dup("VOLATILE2");
  filterComplete4.tagList[1] = CORBA::string_dup("STATIC");
  filterComplete4.tagList[2] = CORBA::string_dup("UNIQUE2");
  filterComplete4.componentList.length(2);
  filterComplete4.componentList[0] = CORBA::string_dup("component 0");
  filterComplete4.componentList[1] = CORBA::string_dup("component 4");

  // create testResults
  bool* testResult[TEST_COUNT];
  for (int i = 0; i < TEST_COUNT; i++) {
    testResult[i] = new bool(true);
  }

  // Start tests...

  // Test 0 is not used

  printf("Test 1: Simple connection and disconnection\n");
  // - connect tools
  // - define filters, remove filters
  // - disconnect tools
  // - connect components
  // - disconnect components
  // system-, component- and toolstate is unchanged(empty) after this test

  printf("Test 1: Phase 1\n");
  testTool[0].checkConnect("tool 0", LS_OK, testResult[1]);
  testTool[0].checkAddFilter(filterComponent[0], testResult[1]);
  testTool[1].checkConnect("", LS_OK, testResult[1]);
  testTool[1].checkAddFilter(filterComponent[1], testResult[1]);
  testTool[0].checkAddFilter(filterComponent[2], testResult[1]);
  testTool[1].checkRemoveFilter("filterComponent 1", testResult[1]);
  testTool[1].checkAddFilter(filterComponent[1], testResult[1]);
  testTool[1].checkRemoveFilter("filterComponent 1", testResult[1]);
  testTool[0].checkFlushAllFilters(testResult[1]);
  testTool[0].checkFlushAllFilters(testResult[1]);  // call Flush on empty list
  testTool[1].checkFlushAllFilters(testResult[1]);  // call Flush on empty list
  testTool[1].checkDisconnect(testResult[1]);
  testTool[0].checkAddFilter(filterComponent[2], testResult[1]);
  testTool[0].checkDisconnect(testResult[1]);   // one filter still exists
  sleep(1);   // let msgs pass through the system
  testTool[0].checkMsgBuf(testResult[1]);  // Buffers should be empty
  testTool[1].checkMsgBuf(testResult[1]);  // Buffers should be empty

  printf("Test 1: Phase 2\n");
  testComp[0].checkConnect("component 0", "myHost", "connectMsg",
                           LS_OK, initialConfig, testResult[1]);
  testComp[1].checkConnect("", "myHost", "connectMsg",
                           LS_OK, initialConfig, testResult[1]);
  testComp[0].checkDisconnect("disconnectMsg", testResult[1]);
  testComp[1].checkDisconnect("disconnectMsg", testResult[1]);
  testComp[1].checkConnect("", "myHost", "connectMsg",
                           LS_OK, initialConfig, testResult[1]);
  testComp[1].checkDisconnect("disconnectMsg", testResult[1]);
  sleep(1);  // let msgs pass through the system

  if (*(testResult[1])) {
    printf("Test 1 passed\n");
  } else {
    printf("Test 1 failed\n");
  }

  printf("Test 2: Test StateManager\n");
  // - connect tool 0
  // - connect tool 1 with filterAll
  // - connect component 0,1
  // - send several Messages
  // - check if tool 0 has received all stateImportant messages
  // - check if tool 1 has received all messages
  // - connect tool 2
  // - check if tool 2 received all stateImportant messages
  // - disconnect tool 1,2
  // - disconnect component 0,1
  // - check if tool 0 has received all stateImportant messages
  // - connect tool 1
  // - check that tool 1 receives no initial messages
  // - disconnect tool 0,1
  // system-, component- and toolstate is unchanged (empty) after this test
  printf("Test 2: Phase 1\n");
  testTool[0].checkConnect("tool 0", LS_OK, testResult[2]);
  testTool[1].checkConnect("tool 1", LS_OK, testResult[2]);
  testTool[1].checkAddFilter(filterAll, testResult[2]);
  testComp[0].checkConnect("component 0", "myHost", "connectMsg",
                           LS_OK, allConfig, testResult[2]);
  testComp[1].checkConnect("component 1", "myHost", "connectMsg",
                           LS_OK, allConfig, testResult[2]);
  testTool[0].addExpectedMsg("component 0", "connectMsg", "IN");
  testTool[0].addExpectedMsg("component 1", "connectMsg", "IN");
  testTool[1].addExpectedMsg("component 0", "connectMsg", "IN");
  testTool[1].addExpectedMsg("component 1", "connectMsg", "IN");
  sleep(1);
  testTool[0].checkMsgBuf(testResult[2]);
  testTool[1].checkMsgBuf(testResult[2]);

  printf("Test 2: Phase 2\n");
  testComp[0].addMessage("DYNAMIC_START", "service 1");
  testTool[0].addExpectedMsg("component 0", "service 1", "DYNAMIC_START");
  testTool[1].addExpectedMsg("component 0", "service 1", "DYNAMIC_START");
  testComp[0].sendMsgBuf();

  testComp[1].addMessage("STATIC", "persistent information");
  testTool[0].addExpectedMsg("component 1", "persistent information",
                             "STATIC");
  testTool[1].addExpectedMsg("component 1", "persistent information",
                             "STATIC");

  testComp[1].addMessage("VOLATILE1", "volatile information");
  testTool[1].addExpectedMsg("component 1", "volatile information",
                             "VOLATILE1");

  testComp[1].addMessage("VOLATILE2", "more volatile information");
  testTool[1].addExpectedMsg("component 1", "more volatile information",
                             "VOLATILE2");
  testComp[1].sendMsgBuf();

  testComp[0].addMessage("UNIQUE1", "information A, version 1");
  testTool[0].addExpectedMsg("component 0", "information A, version 1",
                             "UNIQUE1");
  testTool[1].addExpectedMsg("component 0", "information A, version 1",
                             "UNIQUE1");

  testComp[0].addMessage("UNIQUE2", "information B, version 1");
  testTool[0].addExpectedMsg("component 0", "information B, version 1",
                             "UNIQUE2");
  testTool[1].addExpectedMsg("component 0", "information B, version 1",
                             "UNIQUE2");
  testComp[0].sendMsgBuf();

  testComp[1].addMessage("VOLATILE1", "and more volatile information");
  testTool[1].addExpectedMsg("component 1", "and more volatile information",
                             "VOLATILE1");

  testComp[1].addMessage("STATIC", "more persistent information");
  testTool[0].addExpectedMsg("component 1", "more persistent information",
                             "STATIC");
  testTool[1].addExpectedMsg("component 1", "more persistent information",
                             "STATIC");
  testComp[1].sendMsgBuf();

  testComp[0].addMessage("UNIQUE1", "information A, version 2");
  testTool[0].addExpectedMsg("component 0", "information A, version 2",
                             "UNIQUE1");
  testTool[1].addExpectedMsg("component 0", "information A, version 2",
                             "UNIQUE1");

  testComp[0].addMessage("DYNAMIC_STOP", "service 1");
  testTool[0].addExpectedMsg("component 0", "service 1", "DYNAMIC_STOP");
  testTool[1].addExpectedMsg("component 0", "service 1", "DYNAMIC_STOP");

  testComp[0].addMessage("VOLATILE3", "another useless volatile info");
  testTool[1].addExpectedMsg("component 0", "another useless volatile info",
                             "VOLATILE3");
  testComp[0].sendMsgBuf();

  sleep(2);
  testTool[0].checkMsgBuf(testResult[2]);
  testTool[1].checkMsgBuf(testResult[2]);

  printf("Test 2: Phase 3\n");
  testTool[2].checkConnect("tool 2", LS_OK, testResult[2]);
  testTool[2].addExpectedMsg("component 0", "connectMsg", "IN");
  testTool[2].addExpectedMsg("component 1", "connectMsg", "IN");
  testTool[2].addExpectedMsg("component 1", "persistent information",
                             "STATIC");
  testTool[2].addExpectedMsg("component 0", "information B, version 1",
                             "UNIQUE2");
  testTool[2].addExpectedMsg("component 1", "more persistent information",
                             "STATIC");
  testTool[2].addExpectedMsg("component 0", "information A, version 2",
                             "UNIQUE1");
  sleep(1);
  testTool[2].checkMsgBuf(testResult[2]);

  printf("Test 2: Phase 4\n");
  // everything is connected, statebuffer is filled
  testTool[1].checkDisconnect(testResult[2]);
  testTool[2].checkDisconnect(testResult[2]);

  testComp[0].checkDisconnect("disconnectMsg", testResult[2]);
  testComp[1].checkDisconnect("disconnectMsg", testResult[2]);

  testTool[0].addExpectedMsg("component 0", "disconnectMsg", "OUT");
  testTool[0].addExpectedMsg("component 1", "disconnectMsg", "OUT");

  sleep(1);    // FIXME: without this sleep, we have a minor synchro problem

  printf("Test2: Phase 5\n");

  testTool[1].checkConnect("tool 1", LS_OK, testResult[2]);

  sleep(1);
  testTool[0].checkMsgBuf(testResult[2]);
  testTool[1].checkMsgBuf(testResult[2]);
  testTool[2].checkMsgBuf(testResult[2]);

  testTool[0].checkDisconnect(testResult[2]);
  testTool[1].checkDisconnect(testResult[2]);

  if (*(testResult[2])) {
    printf("Test 2 passed\n");
  } else {
    printf("Test 2 failed\n");
  }


  printf("Test 3: Build up testsystem\n");
  // - connect component 0,1,2
  // - connect tool 0,1,2
  // leave system in this state, all buffers cleared ...
  testTool[0].checkConnect("tool 0", LS_OK, testResult[3]);
  testTool[1].checkConnect("tool 1", LS_OK, testResult[3]);
  testTool[2].checkConnect("tool 2", LS_OK, testResult[3]);

  testComp[0].checkConnect("component 0", "myHost", "connectMsg",
                           LS_OK, initialConfig, testResult[3]);
  testComp[1].checkConnect("component 1", "myHost", "connectMsg",
                           LS_OK, initialConfig, testResult[3]);
  testComp[2].checkConnect("component 2", "myHost", "connectMsg",
                           LS_OK, initialConfig, testResult[3]);
  testTool[0].addExpectedMsg("component 0", "connectMsg", "IN");
  testTool[1].addExpectedMsg("component 0", "connectMsg", "IN");
  testTool[2].addExpectedMsg("component 0", "connectMsg", "IN");
  testTool[0].addExpectedMsg("component 1", "connectMsg", "IN");
  testTool[1].addExpectedMsg("component 1", "connectMsg", "IN");
  testTool[2].addExpectedMsg("component 1", "connectMsg", "IN");
  testTool[0].addExpectedMsg("component 2", "connectMsg", "IN");
  testTool[1].addExpectedMsg("component 2", "connectMsg", "IN");
  testTool[2].addExpectedMsg("component 2", "connectMsg", "IN");

  sleep(1);
  testTool[0].checkMsgBuf(testResult[3]);
  testTool[1].checkMsgBuf(testResult[3]);
  testTool[2].checkMsgBuf(testResult[3]);

  if (*(testResult[3])) {
    printf("Test 3 passed\n");
  } else {
    printf("Test 3 failed\n");
  }


  printf("Test 4: Test FilterManager\n");
  // add filters. check (re)configuration

  testTool[0].checkAddFilter(filterAll, testResult[4]);
  testComp[0].setExpectedConfig(allConfig);
  testComp[1].setExpectedConfig(allConfig);
  testComp[2].setExpectedConfig(allConfig);
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[0].checkRemoveFilter("filterAll", testResult[4]);
  testComp[0].setExpectedConfig(initialConfig);
  testComp[1].setExpectedConfig(initialConfig);
  testComp[2].setExpectedConfig(initialConfig);
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[0].checkAddFilter(filterComponent[0], testResult[4]);
  testComp[0].setExpectedConfig(allConfig);
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[0].checkAddFilter(filterVolatile[1], testResult[4]);
  testComp[1].addExpectedConfig("VOLATILE1");
  testComp[2].addExpectedConfig("VOLATILE1");
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[1].checkAddFilter(filterVolatile[2], testResult[4]);
  testComp[1].addExpectedConfig("VOLATILE2");
  testComp[2].addExpectedConfig("VOLATILE2");
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[1].checkAddFilter(filterComponent[1], testResult[4]);
  testComp[1].setExpectedConfig(allConfig);
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[0].checkFlushAllFilters(testResult[4]);
  testComp[0].setExpectedConfig(initialConfig);
  testComp[0].addExpectedConfig("VOLATILE2");
  testComp[2].setExpectedConfig(initialConfig);
  testComp[2].addExpectedConfig("VOLATILE2");
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[1].checkRemoveFilter("filterVolatile 2", testResult[4]);
  testComp[0].setExpectedConfig(initialConfig);
  testComp[2].setExpectedConfig(initialConfig);
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[1].checkAddFilter(filterC01T12, testResult[4]);
  testComp[0].addExpectedConfig("VOLATILE1");
  testComp[0].addExpectedConfig("VOLATILE2");
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[1].checkAddFilter(filterC12T23, testResult[4]);
  testComp[2].addExpectedConfig("VOLATILE2");
  testComp[2].addExpectedConfig("VOLATILE3");
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[1].checkRemoveFilter("filterComponent 1", testResult[4]);
  testComp[1].setExpectedConfig(initialConfig);
  testComp[1].addExpectedConfig("VOLATILE1");
  testComp[1].addExpectedConfig("VOLATILE2");
  testComp[1].addExpectedConfig("VOLATILE3");
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  testTool[1].checkFlushAllFilters(testResult[4]);
  testComp[0].setExpectedConfig(initialConfig);
  testComp[1].setExpectedConfig(initialConfig);
  testComp[2].setExpectedConfig(initialConfig);
  testComp[0].checkConfig(testResult[4]);
  testComp[1].checkConfig(testResult[4]);
  testComp[2].checkConfig(testResult[4]);

  // guarantee that the tools have not received any message
  testTool[0].checkMsgBuf(testResult[4]);
  testTool[1].checkMsgBuf(testResult[4]);
  testTool[2].checkMsgBuf(testResult[4]);

  if (*(testResult[4])) {
    printf("Test 4 passed\n");
  } else {
    printf("Test 4 failed\n");
  }

  printf("Test 5: Check message filtering\n");
  // - connect comp 3,4 tool 3,4
  // - configure filters. Send messages
  // - disconnect comp 3,4 tool 3,4
  // just test the actual filtering. state-important messages are
  // already tested in Test 1

  testTool[3].checkConnect("tool 3", LS_OK, testResult[5]);
  testTool[4].checkConnect("tool 4", LS_OK, testResult[5]);
  testComp[3].checkConnect("component 3", "myHost", "connectMsg",
                           LS_OK, initialConfig, testResult[5]);
  testComp[4].checkConnect("component 4", "myHost", "connectMsg",
                           LS_OK, initialConfig, testResult[5]);

  // clear ToolReceivedBuffers
  for (int i = 0; i < 3; i++) {
    testTool[i].addExpectedMsg("component 3", "connectMsg", "IN");
    testTool[i].addExpectedMsg("component 4", "connectMsg", "IN");
  }
  for (int i = 3; i < 5; i++) {
    testTool[i].addExpectedMsg("component 0", "connectMsg", "IN");
    testTool[i].addExpectedMsg("component 1", "connectMsg", "IN");
    testTool[i].addExpectedMsg("component 2", "connectMsg", "IN");
    testTool[i].addExpectedMsg("component 3", "connectMsg", "IN");
    testTool[i].addExpectedMsg("component 4", "connectMsg", "IN");
  }
  sleep(1);
  testTool[0].checkMsgBuf(testResult[5]);
  testTool[1].checkMsgBuf(testResult[5]);
  testTool[2].checkMsgBuf(testResult[5]);
  testTool[3].checkMsgBuf(testResult[5]);
  testTool[4].checkMsgBuf(testResult[5]);

  // empty tool. define no filters. will get no msgs
  testTool[0].checkFlushAllFilters(testResult[5]);

  // normal tool. defines two non-overlapping filters and a useLess tags filter
  testTool[1].checkAddFilter(filterC01T12, testResult[5]);
  testTool[1].checkAddFilter(filterComponent[3], testResult[5]);
  testTool[1].checkAddFilter(filterUseless, testResult[5]);

  // overkill tool. define many overlapping filters that cover all
  testTool[2].checkAddFilter(filterC01T12, testResult[5]);
  testTool[2].checkAddFilter(filterC12T23, testResult[5]);
  testTool[2].checkAddFilter(filterComponent[0], testResult[5]);
  testTool[2].checkAddFilter(filterComponent[2], testResult[5]);
  testTool[2].checkAddFilter(filterVolatile[1], testResult[5]);
  testTool[2].checkAddFilter(filterVolatile[3], testResult[5]);
  testTool[2].checkAddFilter(filterC34T12, testResult[5]);

  // define complete, non overlapping filterset. will get all msgs
  testTool[3].checkAddFilter(filterComponent[0], testResult[5]);
  testTool[3].checkAddFilter(filterComponent[1], testResult[5]);
  testTool[3].checkAddFilter(filterComponent[2], testResult[5]);
  testTool[3].checkAddFilter(filterComponent[3], testResult[5]);
  testTool[3].checkAddFilter(filterComponent[4], testResult[5]);

  // define complete, non overlapping filterset. will get all msgs
  testTool[4].checkAddFilter(filterComplete1, testResult[5]);
  testTool[4].checkAddFilter(filterComplete2, testResult[5]);
  testTool[4].checkAddFilter(filterComplete3, testResult[5]);
  testTool[4].checkAddFilter(filterComplete4, testResult[5]);

  // we have 15 tag/component combinations to check, but the bigger
  // part can be done automatically:
  for (int i = 0; i < 5; i++) {    // for all components (0-4)
    snprintf(strBuf, 50, "component %d",i);
    for (int j = 1; j < 4; j++) {    // for all tags (1-3)
      snprintf(strBuf2, 50, "VOLATILE%d", j);
      testComp[i].addMessage(strBuf2, "some information");
      // testTool[0] will never receive any msg
      // testTool[1] must be configured manually
      testTool[2].addExpectedMsg(strBuf, "some information", strBuf2);
      testTool[3].addExpectedMsg(strBuf, "some information", strBuf2);
      testTool[4].addExpectedMsg(strBuf, "some information", strBuf2);
    }
    testComp[i].sendMsgBuf();
  }
  testTool[1].addExpectedMsg("component 0", "some information", "VOLATILE1");
  testTool[1].addExpectedMsg("component 0", "some information", "VOLATILE2");
  testTool[1].addExpectedMsg("component 1", "some information", "VOLATILE1");
  testTool[1].addExpectedMsg("component 1", "some information", "VOLATILE2");
  testTool[1].addExpectedMsg("component 3", "some information", "VOLATILE1");
  testTool[1].addExpectedMsg("component 3", "some information", "VOLATILE2");
  testTool[1].addExpectedMsg("component 3", "some information", "VOLATILE3");

  sleep(2);

  testTool[0].checkMsgBuf(testResult[5]);
  testTool[1].checkMsgBuf(testResult[5]);
  testTool[2].checkMsgBuf(testResult[5]);
  testTool[3].checkMsgBuf(testResult[5]);
  testTool[4].checkMsgBuf(testResult[5]);


  if (*(testResult[5])) {
    printf("Test 5 passed\n");
  } else {
    printf("Test 5 failed\n");
  }

  // Test 6: Send useless stuff, undefined messages, ...  ???
  // - send unwanted messages

  // TODO: write this test
  printf("Test 6: \"real life test\"\n");

  // (this is not testTool[0] specific)
  testTool[0].checkAddFilter("", filterAll, LS_TOOL_ADDFILTER_TOOLNOTEXISTS,
                             testResult[6]);

  filter_t undefinedFilter;
  testTool[0].checkAddFilter(undefinedFilter, testResult[6]);
  testComp[0].addMessage("VOLATILE1", "some information");
  testComp[0].sendMsgBuf();
  testTool[0].checkMsgBuf(testResult[6]);



  if (*(testResult[6])) {
    printf("Test 6 passed\n");
  } else {
    printf("Test 6 failed\n");
  }



  printf("Test 7: shutdown everything properly\n");

  testTool[0].checkDisconnect(testResult[7]);
  testTool[1].checkDisconnect(testResult[7]);
  testTool[2].checkDisconnect(testResult[7]);
  testTool[3].checkDisconnect(testResult[7]);
  testTool[4].checkDisconnect(testResult[7]);

  testComp[0].checkDisconnect("disconnectMsg", testResult[7]);
  testComp[1].checkDisconnect("disconnectMsg", testResult[7]);
  testComp[2].checkDisconnect("disconnectMsg", testResult[7]);
  testComp[3].checkDisconnect("disconnectMsg", testResult[7]);
  testComp[4].checkDisconnect("disconnectMsg", testResult[7]);

  // now just make sure, that the stateManager is correct
  testTool[0].checkConnect("", LS_OK, testResult[7]);
  testTool[0].checkMsgBuf(testResult[7]);
  sleep(1);
  testTool[0].checkDisconnect(testResult[7]);

  if (*(testResult[7])) {
    printf("Test 7 passed\n");
  } else {
    printf("Test 7 failed\n");
  }





  // evaluate tests
  bool allOK = true;
  printf("\nTestsuite completed:\n");
  for (int i = 0; i < TEST_COUNT; i++) {
    if (*(testResult[i]) == false) {
      printf("failed test %d\n", i);
      allOK = false;
    }
    delete(testResult[i]);
  }
  if (allOK) {
    printf("All tests passed successfully\n");
  }

  printf("\nShutting down...\n");

  /**
   * Stop threads
   */
  printf("Stop coreThread\n");
  coreThread->stopThread();
  printf("Stop sendThread\n");
  sendThread->stopThread();

  /**
   * Shutdown and delete the orb
   */
  printf("Shutdown the ORB\n");
  orb->shutdown(true);
  printf("Destroy the ORB\n");
  orb->destroy();

  /**
   * Delete internal components (Servants are deleted by the ORB)
   */

  printf("Delete the TimeBuffer\n");
  delete timeBuffer;
  printf("Delete the SimpleFilterManager\n");
  delete simpleFilterManager;
  printf("Delete the StateManager\n");
  delete stateManager;
  printf("Delete the ComponentList\n");
  delete componentList;
  printf("Delete the ToolList\n");
  delete toolList;

  printf("All done\n");
}
