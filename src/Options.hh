/****************************************************************************/
/* DIET forwarder implementation - Executable options                       */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Gael Le Mahec   (gael.le.mahec@ens-lyon.fr)                         */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
#ifndef OPTIONS_HH
#define OPTIONS_HH

#include <list>
#include <map>
#include <string>

class Options;

/* Standard configuration class. Used as an abstract class for parameters
 * processing.
 */
class Configuration {
public:
  Configuration();

  explicit Configuration(const std::string& pgName);

  const std::string&
  getPgName() const;

  const std::string&
  getConfigFile() const;

  void
  setConfigFile(const std::string& configFile);

private:
  std::string pgName;
  std::string configFile;
};

/* Callback function type definition. */
typedef void (*optCallback)(const std::string&, Configuration*);

/* Options class. Used to process the users command line parameters. */
/* This class is a generic command line parameters processing tool.
 */
class Options {
public:
  Options(Configuration* config, int argc, char* argv[], char* envp[] = NULL);

  void
  setOptCallback(const std::string& arg, optCallback callBack);

  void
  setEnvCallback(const std::string& arg, optCallback callBack);

  void
  setParamCallback(unsigned int idx, optCallback callBack);

  void
  setFlagCallback(const char flag, optCallback callBack);

  void
  processOptions();

  void
  processEnv();

private:
  Configuration* config;
  std::map<std::string, std::string> arguments;
  std::map<std::string, std::string> environment;
  std::map<unsigned int, std::string> params;
  std::list<std::string> singleArgs;
  std::list<std::string> singleEnvs;
  std::list<char> flags;
  std::map<std::string, optCallback> optCallbacks;
  std::map<std::string, optCallback> envCallbacks;
  std::map<unsigned int, optCallback> paramCallbacks;
  std::map<char, optCallback> flagCallbacks;
};

/* A simple configuration file class.
 * The file must respect the format:
 * <attribute> = <value>
 */
class ConfigFile {
public:
  ConfigFile();

  explicit ConfigFile(const std::string& path);

  void
  parseFile(const std::string& path);

  const std::string&
  getAttr(const std::string& key);

private:
  std::map<std::string, std::string> attributes;
};
#endif
