/****************************************************************************/
/* Log forwarder implementation                                             */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Gael Le Mahec   (gael.le.mahec@ens-lyon.fr)                         */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/

#ifndef LOGFORWARDER_HH
#define LOGFORWARDER_HH

#include <list>
#include <map>
#include <string>
#include <omnithread.h>
#include "CorbaLogForwarder.hh"
#include "commonLogTypes.hh"


class LogForwarder : public POA_CorbaLogForwarder,
                     public PortableServer::RefCountServantBase {
public:
  ~LogForwarder();

  explicit LogForwarder(const std::string& name);
  /* DIET object factory methods. */

  ComponentConfigurator_ptr
  getCompoConf(const char* name);

  ToolMsgReceiver_ptr
  getToolMsgReceiver(const char* name);

  LogCentralComponent_ptr
  getLogCentralComponent(const char* name);

  LogCentralTool_ptr
  getLogCentralTool(const char* name);

  /* Common methods implementations. */
  void
  ping(const char* compoName, const char* objName);

  /* To determine if the call is from another forwarder and
   * modify the object name.
   */
  static bool
  remoteCall(std::string& objName);

  /* CORBA remote management implementation. */
  void
  bind(const char* objName, const char* ior);

  void
  unbind(const char* objName);

  LogSeqString*
  getBindings(const char* ctxt);

  /* Connect the peer forwarder. */
  void
  connectPeer(const char* ior, const char* host, const ::CORBA::Long port);

  /* Set this forwarder peer object (not CORBA). */
  void
  setPeer(CorbaLogForwarder_ptr peer);

  CorbaLogForwarder_var
  getPeer();

  char*
  getIOR();

  /* Object caches management functions. */
  void
  removeObjectFromCache(const std::string& name);

  void
  cleanCaches();

  char*
  getName();

  char*
  getPeerName();

  char*
  getHost();

  char*
  getPeerHost();

  LogSeqString* routeTree();

  /* Utility function. */
  std::list<std::string>
  otherForwarders() const;

  /* LogComponentFwdr implementation. */
  void
  setTagFilter(const ::tag_list_t& tagList, const char* objName);

  void
  addTagFilter(const ::tag_list_t& tagList, const char* objName);

  void
  removeTagFilter(const ::tag_list_t& tagList, const char* objName);

  void
  test(const char* objName);

  /**
   * Disconnects a connected tool from the monitor. No further
   * filterconfigurations should be sent after this call. The
   * toolMsgReceiver will not be used by the monitor any more
   * after this call. Returns NOTCONNECTED if the calling tool
   * was not connected.
   */
  short
  disconnectTool(const char* toolName, const char* objName);

  /**
   * Returns a list of possible tags. This is just a convenience
   * functions and returns the values that are specified in a
   * configuration file. If the file is not up to date, the
   * application may generate more tags than defined in this
   * list.
   */
  tag_list_t*
  getDefinedTags(const char* objName);

  /**
   * Returns a list of actually connected Components. This is just
   * a convenience function, as the whole state of the system will
   * be sent to the tool right after connection (in the form of
   * messages)
   */
  component_list_t*
  getDefinedComponents(const char*  objName);


  /**
   * Create a filter for this tool on the monitor. Messages matching
   * this filter will be forwarded to the tool. The filter will be
   * identified by its name, which is a part of filter_t. A tool
   * can have as much filters as it wants. Returns ALREADYEXISTS if
   * another filter with this name is already registered.
   */
  short
  addFilter(const char* toolName, const filter_t& filter, const char* objName);


  void
  sendMsg(const log_msg_buf_t& msgBuf, const char*  objName);

  /**
   * Connect a Tool with its toolName, which must be unique among all
   * tools. The return value indicates the success of the connection.
   * If ALREADYEXISTS is returned, the tool could not be attached, as
   * the specified toolName already exists. In this case the tool must
   * reconnect with another name before specifying any filters. If the
   * tool sends an empty toolName, the LogCentral will provide a unique
   * toolName and pass it back to the tool.
   */
  short
  connectTool(char*& toolName, const char* msgReceiver,  const char* objName);

  short
  flushAllFilters(const char* toolName, const char* objName);

  short
  removeFilter(const char* toolName, const char* filterName,
               const char* objName);



  /* Utility fonctions to extract name & context from context/name. */
  static std::string
  getName(const std::string& namectxt);

  static std::string
  getCtxt(const std::string& namectxt);

  short
  connectComponent(char*&, const char*, const char*, const char*,
                   const log_time_t&, tag_list_t&, const char*);


  short
  disconnectComponent(const char* componentName, const char* message,
                      const char* objName);


  void
  sendBuffer(const log_msg_buf_t &buffer, const char* objName);


  void
  synchronize(const char* componentName, const log_time_t& componentTime,
              const char* objName);

private:
  /* When a new forwarder object is created, we cache it.
   * Because this kind of object contains only the object
   * name and a reference to this forwarder, there is no
   * risk to cache it, even if the object is restarted or
   * disappear.
   */
  std::map<std::string, ::CORBA::Object_ptr> objectCache;
  /* We also maintain a list of activated servant objects. */
  std::map<std::string, PortableServer::ServantBase*> servants;

  ::CORBA::Object_ptr
  getObjectCache(const std::string& name);

  /* The forwarder associated to this one. */
  CorbaLogForwarder_var peer;
  /* Mutexes */
  omni_mutex peerMutex;    // To wait for the peer initialization
  omni_mutex cachesMutex;  // Protect access to caches

  std::string peerName;
  std::string name;
  std::string host;
};

#endif
