/****************************************************************************/
/* Log debug utils header                                                  */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Philippe COMBES (Philippe.Combes@ens-lyon.fr)                       */
/*    - Frederic LOMBARD (Frederic.Lombard@lifc.univ-fcomte.fr)             */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/

#ifndef _DEBUG_HH_
#define _DEBUG_HH_

#include <cmath> /* include used for the definition of HUGE_VAL*/
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <sys/time.h>
#include <unistd.h>
#include <omniconfig.h>
#include <omnithread.h>


/** The trace level. */
extern unsigned int TRACE_LEVEL;
/** mutex used by the debug library to share the stderr and stdout */
extern omni_mutex debug_log_mutex;

/**
 * Various values for the trace level
 */
#define NO_TRACE            0
#define TRACE_ERR_AND_WARN  1
#define TRACE_MAIN_STEPS    2
#define TRACE_ALL_STEPS     5
#define TRACE_STRUCTURES   10
#define TRACE_MAX_VALUE    TRACE_STRUCTURES
#define TRACE_DEFAULT      TRACE_MAIN_STEPS

/**
 * Definition of the EXIT_FUNCTION when not yet defined.
 * You must place the EXIT_FUNCTION's definition before any
 * preprocessing inclusion for this to work.
 */
#ifndef EXIT_FUNCTION
#define EXIT_FUNCTION \
  std::cout << "DEBUG WARNING: EXIT_FUNCTION undeclared !\n"
#endif


/**
 * Always useful
 */
#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif /* MIN */
#ifndef MAX
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif  /* MAX */

/**
 * Print a the name of the file and the line number where this macro
 * is put to stdout.
 */
#define BIP printf("bip - " __FILE__ " %i\n", __LINE__)

/**
 * Error message - return with return_value.
 */
#define ERROR(formatted_msg, return_value) {                             \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(TRACE_ERR_AND_WARN)) { \
    debug_log_mutex.lock();                                             \
    std::cerr << "LOG ERROR: " << formatted_msg << ".\n";               \
    debug_log_mutex.unlock();                                           \
  }                                                                     \
  return return_value; }

#define ERROR_EXIT(formatted_msg) {                                     \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(TRACE_ERR_AND_WARN)) { \
    debug_log_mutex.lock();                                             \
    std::cerr << "LOG ERROR: " << formatted_msg << ".\n";               \
    debug_log_mutex.unlock();                                           \
  }                                                                     \
  exit(1); }

#define INTERNAL_ERROR_EXIT(formatted_msg) {                            \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(TRACE_ERR_AND_WARN)) { \
    debug_log_mutex.lock();                                             \
    std::cerr << "LOG ERROR: " << formatted_msg << ".\n";               \
    debug_log_mutex.unlock();                                           \
  }                                                                     \
  exit(1); }

/**
 * Warning message.
 */
#define WARNING(formatted_msg)                                          \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(TRACE_ERR_AND_WARN)) { \
    debug_log_mutex.lock();                                             \
    std::cerr << "LOG WARNING: " << formatted_msg << ".\n";             \
    debug_log_mutex.unlock(); }


/**
 * Internal Error message - exit with exit_value.
 */
#define INTERNAL_ERROR(formatted_msg, exit_value)                       \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(TRACE_ERR_AND_WARN)) { \
    debug_log_mutex.lock();                                             \
    std::cerr << "LOG INTERNAL ERROR: " << formatted_msg << ".\n" <<    \
      "Please send bug report to diet-usr@ens-lyon.fr\n";               \
    debug_log_mutex.unlock(); }                                         \
  EXIT_FUNCTION;                                                        \
  exit(exit_value)

/**
 * Internal Warning message.
 */
#define INTERNAL_WARNING(formatted_msg)                                 \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(TRACE_ERR_AND_WARN)) { \
    debug_log_mutex.lock();                                             \
    std::cerr << "LOG INTERNAL WARNING: " << formatted_msg << ".\n"     \
              << "This is not a fatal bug, but please send a report "   \
      "to diet-dev@ens-lyon.fr\n";                                      \
    debug_log_mutex.unlock(); }


// DEBUG pause: insert a pause of duration <s>+<us>E-6 seconds
#define PAUSE(s, us)                            \
  {                                             \
    struct timeval tv;                          \
    tv.tv_sec  = s;                             \
    tv.tv_usec = us;                            \
    select(0, NULL, NULL, NULL, &tv);           \
  }


// DEBUG trace: print "function(formatted_text)\n", following the iostream
// format. First argument is the minimum TRACE_LEVEL for the line to be printed.
#define TRACE_FUNCTION(level, formatted_text)                           \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(level)) {       \
    debug_log_mutex.lock();                                             \
    std::cout << __FUNCTION__ << '(' << formatted_text << ")\n";        \
    debug_log_mutex.unlock(); }

// DEBUG trace: print formatted_text following the iostream format (no '\n'
// added). First argument is the minimum TRACE_LEVEL for the line to be printed.
#define TRACE_TEXT(level, formatted_text)                               \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(level)) {       \
    debug_log_mutex.lock();                                             \
    std::cout << formatted_text;                                        \
    debug_log_mutex.unlock(); }

// DEBUG trace: print "file:line: formatted_text", following the iostream format
// (no '\n' added). First argument is the minimum TRACE_LEVEL for the line to be
// printed.
#define TRACE_TEXT_POS(level, formatted_text)                           \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(level)) {       \
    debug_log_mutex.lock();                                             \
    std::cout << __FILE__ << ':' << __LINE__ << ": " << formatted_text; \
    debug_log_mutex.unlock(); }

// DEBUG trace: print "time: formatted_text", following the iostream format (no
// '\n' added). First argument is the minimum TRACE_LEVEL for the line to be
// printed.
#define TRACE_TIME(level, formatted_text)                               \
  if (static_cast<int>(TRACE_LEVEL) >= static_cast<int>(level)) {       \
    struct timeval tv;                                                  \
    debug_log_mutex.lock();                                             \
    gettimeofday(&tv, NULL);                                            \
    printf("%10ld.%06ld: ",                                             \
           (unsigned long)tv.tv_sec, (unsigned long)tv.tv_usec);        \
    std::cout << formatted_text;                                        \
    debug_log_mutex.unlock();                                           \
  }

// DEBUG trace: print variable name and value
#define TRACE_VAR(var) {                                        \
    debug_log_mutex.lock();                                     \
    TRACE_TEXT_POS(NO_TRACE, #var << " = " << (var) << "\n");   \
    debug_log_mutex.unlock(); }



#endif  // _DEBUG_HH_
