/****************************************************************************/
/* A class for putting some ORB functions together - IMPLEMENTATION         */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.5  2011/02/07 12:27:42  bdepardo
 * Correctly declare tracelevel option
 *
 * Revision 1.4  2008/07/17 01:03:12  rbolze
 * make some change to avoid gcc warning
 *
 * Revision 1.3  2006/02/17 14:46:55  ecaron
 * Bug fix: uncompatible syntax with new(char*)
 *
 * Revision 1.2  2004/06/01 21:45:58  hdail
 * Tracking down seg fault in LogService:
 * - Several strings were created in this file using malloc that are later cleaned
 * up by omniORB code using delete[].  Since creation and deletion approaches must
 * match for some non-linux systems, I removed old method of strdup and added new
 * method of allocation with new char[...] and then copied the data with strcpy.
 *
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#include <cstdio>
#include <iostream>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <omnithread.h>
#include "ORBTools.hh"


CORBA::ORB_var ORBTools::orb = CORBA::ORB::_nil();
PortableServer::POA_var ORBTools::poa = PortableServer::POA::_nil();

bool
ORBTools::init(int argc, char** argv) {
  ORBTools::orb = CORBA::ORB_init(argc, argv);
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  CORBA::Object_var obj = ORBTools::orb->resolve_initial_references("RootPOA");
  if (CORBA::is_nil(obj)) {
    return false;
  }
  ORBTools::poa = PortableServer::POA::_narrow(obj);
  if (CORBA::is_nil(ORBTools::poa)) {
    return false;
  }
  return true;
}

bool
ORBTools::init(int argc, char** argv,
               unsigned int tracelevel, unsigned int port) {
  int myargc = argc;
  char** myargv = NULL;
  if (port == 0) {
    myargc += 2;
    myargv = new char*[myargc];
    for (int i = 0; i < myargc - 2; i++) {
      myargv[i] = strdup(argv[i]);
    }
    myargv[myargc - 2] = strdup("-ORBtraceLevel");
    char* s = new char[6];
    snprintf(s, 6, "%u", tracelevel);
    myargv[myargc - 1] = s;
  } else {
    myargc += 4;
    myargv = new char*[myargc];
    for (int i = 0; i < myargc - 4; i++) {
      myargv[i] = strdup(argv[i]);
    }
    myargv[myargc - 4] = strdup("-ORBtraceLevel");
    char* s1 = new char[6];
    snprintf(s1, 6, "%u", tracelevel);
    myargv[myargc - 3] = s1;
    myargv[myargc - 2] = strdup("-ORBendPoint");
    char* s2 = new char[6];
    snprintf(s2, 6, "%u", port);
    myargv[myargc - 1] = s2;
  }
  ORBTools::orb = CORBA::ORB_init(myargc, myargv);
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  CORBA::Object_var obj = ORBTools::orb->resolve_initial_references("RootPOA");
  if (CORBA::is_nil(obj)) {
    return false;
  }
  ORBTools::poa = PortableServer::POA::_narrow(obj);
  if (CORBA::is_nil(ORBTools::poa)) {
    return false;
  }
  return true;
}

bool
ORBTools::init() {
  int myargc = 0;
  char** myargv = new char*[1];
  myargv[0] = strdup("");
  ORBTools::orb = CORBA::ORB_init(myargc, myargv);
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  CORBA::Object_var obj = ORBTools::orb->resolve_initial_references("RootPOA");
  if (CORBA::is_nil(obj)) {
    return false;
  }
  ORBTools::poa = PortableServer::POA::_narrow(obj);
  if (CORBA::is_nil(ORBTools::poa)) {
    return false;
  }
  return true;
}

bool
ORBTools::registerServant(const char* contextName, const char* contextKind,
                          const char* name, const char* kind,
                          CORBA::Object* objref) {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  if (objref == NULL) {
    return false;
  }
  CosNaming::NamingContext_var rootContext;
  try {
    CORBA::Object_var obj;
    obj = ORBTools::orb->resolve_initial_references("NameService");
    try {
      rootContext = CosNaming::NamingContext::_narrow(obj);
    } catch (CORBA::SystemException& ex) {
      std::cerr << "Failed to narrow the root naming context.\n";
      return false;
    }
  } catch (CORBA::ORB::InvalidName& ex) {
    std::cerr << "The NamingService could not be resolved (InvalidName)\n";
    return false;
  } catch (CORBA::SystemException& ex) {
    std::cerr << "The NamingService could not be resolved (SystemException)\n";
    return false;
  }
  try {
    CosNaming::Name context;
    context.length(1);
    size_t sz1 = strlen(contextName) + 1;
    context[0].id = new char[sz1];
    snprintf(context[0].id, sz1,  contextName);
    size_t sz2 = strlen(contextKind) + 1;
    context[0].kind = new char[sz2];
    snprintf(context[0].kind, sz2, contextKind);
    CosNaming::NamingContext_var testContext;
    try {
      testContext = rootContext->bind_new_context(context);
    } catch (CosNaming::NamingContext::AlreadyBound& ex) {
      CORBA::Object_var obj;
      obj = rootContext->resolve(context);
      testContext = CosNaming::NamingContext::_narrow(obj);
      if (CORBA::is_nil(testContext)) {
        std::cerr << "Failed to narrow naming context.\n";
        return false;
      }
    }
    CosNaming::Name object;
    object.length(1);
    sz1 = strlen(name) + 1;
    object[0].id   = new char[sz1];
    snprintf(object[0].id, sz1, name);
    sz2 = strlen(kind) + 1;
    object[0].kind = new char[sz2];
    snprintf(object[0].kind, sz2, kind);

    try {
      testContext->bind(object, objref);
    } catch (CosNaming::NamingContext::AlreadyBound& ex) {
      testContext->rebind(object, objref);
    }
  } catch (CORBA::COMM_FAILURE& ex) {
    std::cerr << "Caught system exception COMM_FAILURE -- "
      "unable to contact the service.\n";
    return false;
  } catch (CORBA::SystemException& ex) {
    std::cerr << "Caught a CORBA::SystemException "
      "while using the naming service.\n";
    return false;
  }
  return true;
}

bool
ORBTools::registerServant(const char* name, const char* kind,
                          CORBA::Object* objref) {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  if (objref == NULL) {
    return false;
  }
  CosNaming::NamingContext_var rootContext;
  try {
    CORBA::Object_var obj;
    obj = ORBTools::orb->resolve_initial_references("NameService");
    try {
      rootContext = CosNaming::NamingContext::_narrow(obj);
    } catch (CORBA::SystemException& ex) {
      std::cerr << "Failed to narrow the root naming context.\n";
      return false;
    }
  } catch (CORBA::ORB::InvalidName& ex) {
    std::cerr << "The NamingService could not be resolved (InvalidName)\n";
    return false;
  } catch (CORBA::SystemException& ex) {
    std::cerr << "The NamingService could not be resolved (SystemException)\n";
    return false;
  }
  try {
    CosNaming::Name objectName;
    objectName.length(1);
    objectName[0].id   = strdup(name);
    objectName[0].kind = strdup(kind);
    try {
      rootContext->bind(objectName, objref);
    } catch (CosNaming::NamingContext::AlreadyBound& ex) {
      rootContext->rebind(objectName, objref);
    }
  } catch (CORBA::COMM_FAILURE& ex) {
    std::cerr << "Caught system exception COMM_FAILURE -- "
      "unable to contact the service.\n";
    return false;
  } catch (CORBA::SystemException& ex) {
    std::cerr << "Caught a CORBA::SystemException while "
      "using the naming service.\n";
    return false;
  }
  return true;
}

bool
ORBTools::activateServant(PortableServer::ServantBase* object) {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  ORBTools::poa->activate_object(object);
  object->_remove_ref();
  return true;
}

bool
ORBTools::activatePOA() {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  PortableServer::POAManager_var pman = ORBTools::poa->the_POAManager();
  pman->activate();
  return true;
}

bool
ORBTools::unregisterServant(const char* contextName, const char* contextKind,
                            const char* name, const char* kind) {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  CosNaming::NamingContext_var rootContext, context;
  try {
    CORBA::Object_var obj;
    obj = ORBTools::orb->resolve_initial_references("NameService");
    try {
      rootContext = CosNaming::NamingContext::_narrow(obj);
    } catch (CORBA::SystemException& ex) {
      std::cerr << "Failed to narrow the root naming context.\n";
      return false;
    }
  } catch (CORBA::ORB::InvalidName& ex) {
    std::cerr << "Service required is invalid [does not exist].\n";
    return false;
  }
  CosNaming::Name cosName;
  cosName.length(1);
  cosName[0].id = strdup(contextName);
  cosName[0].kind = strdup(contextKind);
  try {
    try {
      CORBA::Object_var tmpobj;
      tmpobj = rootContext->resolve(cosName);
      context = CosNaming::NamingContext::_narrow(tmpobj);
      if (CORBA::is_nil(context)) {
        std::cerr << "Failed to narrow the naming context.\n";
        return false;
      }
    } catch (CosNaming::NamingContext::NotFound& ex) {
      std::cerr << "Cannot find object\n";
      return false;
    } catch (...) {
      std::cerr << "System exception caught while using the naming service\n";
      return false;
    }
    cosName[0].id   = strdup(name);
    cosName[0].kind = strdup(kind);
    context->unbind(cosName);
  } catch (CORBA::COMM_FAILURE& ex) {
    std::cerr << "System exception caught (COMM_FAILURE), unable to connect to "
         << "the CORBA name server\n";
    return false;
  } catch (omniORB::fatalException& ex) {
    throw;
  } catch (...) {
    std::cerr << "System exception caught while using the naming service\n";
    return false;
  }
  return true;
}

bool
ORBTools::unregisterServant(const char* name, const char* kind) {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  CosNaming::NamingContext_var rootContext, context;
  try {
    CORBA::Object_var obj;
    obj = ORBTools::orb->resolve_initial_references("NameService");
    try {
      rootContext = CosNaming::NamingContext::_narrow(obj);
    } catch (CORBA::SystemException& ex) {
      std::cerr << "Failed to narrow the root naming context.\n";
      return false;
    }
  } catch (CORBA::ORB::InvalidName& ex) {
    std::cerr << "Service required is invalid [does not exist].\n";
    return false;
  }
  CosNaming::Name cosName;
  cosName.length(1);
  cosName[0].id = strdup(name);
  cosName[0].kind = strdup(kind);
  try {
    rootContext->unbind(cosName);
  } catch (CORBA::COMM_FAILURE& ex) {
    std::cerr << "System exception caught (COMM_FAILURE),"
      "unable to connect to the CORBA name server\n";
    return false;
  } catch (omniORB::fatalException& ex) {
    throw;
  } catch (...) {
    std::cerr << "System exception caught while using the naming service\n";
    return false;
  }
  return true;
}

bool
ORBTools::findServant(const char* contextName, const char* contextKind,
                      const char* name, const char* kind,
                      CORBA::Object*& objref) {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  objref = NULL;
  CosNaming::NamingContext_var rootContext;
  try {
    CORBA::Object_var obj;
    obj = ORBTools::orb->resolve_initial_references("NameService");
    try {
      rootContext = CosNaming::NamingContext::_narrow(obj);
    } catch (CORBA::SystemException& ex) {
      std::cerr << "Failed to narrow the root naming context.\n";
      return false;
    }
  } catch (CORBA::ORB::InvalidName& ex) {
    std::cerr << "Service required is invalid [does not exist].\n";
    return false;
  }
  CosNaming::Name cosName;
  cosName.length(2);
  cosName[0].id   = contextName;
  cosName[0].kind = contextKind;
  cosName[1].id   = name;
  cosName[1].kind = kind;
  try {
    objref = rootContext->resolve(cosName);
  } catch (CosNaming::NamingContext::NotFound& ex) {
    std::cerr << "Context for " << name << " not found\n";
    return false;
  } catch (CORBA::COMM_FAILURE& ex) {
    std::cerr << "System exception caught (COMM_FAILURE): "
      "unable to connect to CORBA name server.\n";
    return false;
  } catch (omniORB::fatalException& ex) {
    throw;
  } catch (...) {
    std::cerr << "System exception caught while using the naming service\n";
    return false;
  }
  return true;
}

bool
ORBTools::findServant(const char* name, const char* kind,
                      CORBA::Object*& objref) {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  objref = NULL;
  CosNaming::NamingContext_var rootContext;
  try {
    CORBA::Object_var obj;
    obj = ORBTools::orb->resolve_initial_references("NameService");
    try {
      rootContext = CosNaming::NamingContext::_narrow(obj);
    } catch (CORBA::SystemException& ex) {
      std::cerr << "Failed to narrow the root naming context.\n";
      return false;
    }
  } catch (CORBA::ORB::InvalidName& ex) {
    std::cerr << "Service required is invalid [does not exist].\n";
    return false;
  }
  CosNaming::Name cosName;
  cosName.length(1);
  cosName[0].id   = strdup(name);
  cosName[0].kind = strdup(kind);
  try {
    objref = rootContext->resolve(cosName);
  } catch (CosNaming::NamingContext::NotFound& ex) {
    std::cerr << "Context for " << name << " not found\n";
    return false;
  } catch (CORBA::COMM_FAILURE& ex) {
    std::cerr << "System exception caught (COMM_FAILURE): unable to connect to "
              << "the CORBA name server.\n";
    return false;
  } catch (omniORB::fatalException& ex) {
    throw;
  } catch (...) {
    std::cerr << "System exception caught while using the naming service\n";
    return false;
  }
  return true;
}



static void
make_read_block() {
  long file_flags;
  fd_set fs;
  FD_SET(STDIN_FILENO, &fs);
  file_flags = 0;
  fcntl(STDIN_FILENO, F_SETFL, file_flags);
}

static void
make_read_nonblock() {
  long file_flags;
  fd_set fs;
  FD_ZERO(&fs);
  FD_SET(STDIN_FILENO, &fs);
  file_flags = fcntl(STDIN_FILENO, F_GETFL);
  if (file_flags == -1) {
    file_flags = 0;
  }
  fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK|file_flags);
}


bool
ORBTools::listen(char stopLowercase, char stopUppercase) {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  bool again = true;
  char c = '\0';
  make_read_nonblock();
  while (again) {
    omni_thread::sleep(0, 500000000);  // sleep 500ms
    c = fgetc(stdin);
    again = (c != stopLowercase && c != stopUppercase);
  }
  make_read_block();
  return true;
}

bool
ORBTools::kill() {
  if (CORBA::is_nil(ORBTools::orb)) {
    return false;
  }
  ORBTools::orb->shutdown(true);
  ORBTools::orb->destroy();
  return true;
}


