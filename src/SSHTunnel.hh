/****************************************************************************/
/* Log forwarder implementation - SSH Tunnel implementation                 */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Gael Le Mahec   (gael.le.mahec@ens-lyon.fr)                         */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/

#ifndef SSHTUNNEL_HH
#define SSHTUNNEL_HH

#include <string>

#include <csignal>
#include <unistd.h>

class SSHConnection {
public:
  SSHConnection();

  SSHConnection(const std::string& sshHost, const std::string& sshPort,
                const std::string& login, const std::string& keyPath,
                const std::string& sshPath);

  const std::string&
  getSshHost() const;

  const std::string&
  getSshPath() const;

  const std::string&
  getSshPort() const;

  const std::string&
  getSshLogin() const;

  const std::string&
  getSshKeyPath() const;

  const std::string&
  getSshOptions() const;

  void
  setSshHost(const std::string& host);

  void
  setSshPath(const std::string& path);

  void
  setSshPort(const std::string& port);

  void
  setSshPort(const int port);

  void
  setSshLogin(const std::string& login);

  void
  setSshKeyPath(const std::string& path);

  void
  setSshOptions(const std::string& options);
protected:
  /* Get the default user login and private key. */
  static std::string
  userLogin();

  static std::string
  userKey();
private:
  /* SSH executable path. */
  std::string sshPath;
  /* SSH connection params. */
  std::string login;
  std::string keyPath;
  std::string sshHost;
  std::string sshPort;
  std::string options;
};

class SSHTunnel : public SSHConnection {
public:
  SSHTunnel();
  /* Constructor for bi-directionnal SSH tunnel. */
  SSHTunnel(const std::string& sshHost,
            const std::string& remoteHost,
            const std::string& localPortFrom,
            const std::string& remotePortTo,
            const std::string& remotePortFrom,
            const std::string& localPortTo,
            const bool createTo = true,
            const bool createFrom = true,
            const std::string& sshPath = "/usr/bin/ssh",
            const std::string& sshPort = "22",
            const std::string& login = userLogin(),
            const std::string& keyPath = userKey());

  /* Constructor for unidirectionnal SSH tunnel. */
  SSHTunnel(const std::string& sshHost,
            const std::string& remoteHost,
            const std::string& localPortFrom,
            const std::string& remotePortTo,
            const bool createTo = true,
            const std::string& sshPath = "/usr/bin/ssh",
            const std::string& serverPort = "22",
            const std::string& login = userLogin(),
            const std::string& keyPath = userKey());

  ~SSHTunnel();

  void
  open();

  void
  close();

  const std::string&
  getRemoteHost() const;

  int
  getLocalPortFrom() const;

  int
  getLocalPortTo() const;

  int
  getRemotePortFrom() const;

  int
  getRemotePortTo() const;

  void
  setRemoteHost(const std::string& host);

  void
  setLocalPortFrom(const std::string& port);

  void
  setLocalPortFrom(const int port);

  void
  setRemotePortTo(const std::string& port);

  void
  setRemotePortTo(const int port);

  void
  setRemotePortFrom(const std::string& port);

  void
  setRemotePortFrom(const int port);

  void
  setLocalPortTo(const std::string& port);

  void
  setLocalPortTo(const int port);

  void
  setWaitingTime(const unsigned int time);

  void
  createTunnelTo(const bool create);

  void
  createTunnelFrom(const bool create);

private:
  /* Format strings for ssh commands. */
  static std::string cmdFormat;
  static std::string cmdFormatDefault;
  static std::string localFormat;
  static std::string remoteFormat;
  static std::string keyFormat;
  /* Tunnel configuration. */
  bool createFrom;
  bool createTo;
  unsigned int waitingTime;
  std::string localPortTo;
  std::string localPortFrom;
  std::string remoteHost;
  std::string remotePortTo;
  std::string remotePortFrom;

  /* Process pid. */
  pid_t pid;

  std::string
  makeCmd();
};

/* Copy a file using scp. */
class SSHCopy : public SSHConnection {
public:
  SSHCopy(const std::string& sshHost,
          const std::string& remoteFilename,
          const std::string& localFilename);
  bool
  getFile() const;

  bool
  putFile() const;

private:
  std::string remoteFilename;
  std::string localFilename;

  /* Process pid. */
  mutable pid_t pid;
};


std::string
freeTCPport();

#endif
