/****************************************************************************/
/* A sample component based on libLogComponentBase.a                        */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.5  2011/05/13 08:17:52  bdepardo
 * Update ORB manager with changes made in DIET ORB manager.
 *
 * Revision 1.4  2011/05/13 06:44:59  bdepardo
 * Add missing include
 *
 * Revision 1.3  2011/02/04 15:52:22  bdepardo
 * System headers before our own headers
 *
 * Revision 1.2  2010/12/03 12:40:26  kcoulomb
 * MAJ log to use forwarders
 *
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#include <cstdio>
#include <iostream>
#include <unistd.h>

#include "LogComponentBase.hh"
#include "LogORBMgr.hh"


int
main(int argc, char** argv) {
  std::cout << "Test of liblogcomponentbase.a\n\n";
  bool success = false;
  std::cout << "Init \n\n";
  try {
    LogORBMgr::init(argc, argv);
  } catch (...) {
    fprintf(stderr, "ORB initialization failed");
  }
  LogComponentBase* logComponentBase =
    new LogComponentBase(&success, argc, argv, 0, "testComponent");
  std::cout << "Inited \n\n";
  if (!success) {
    std::cerr << "Cannot innitialize the LogComponentBase !\n"
         << "Are LogCentral or Omninames not running ?\n";
    return 1;
  }
  short ret = 0;
  ret = logComponentBase->connect("testCompo connected \\o/");
  if (ret == LS_COMPONENT_CONNECT_BADNAME) {
    std::cerr << "Component : bad name !\n";
    return ret;
  }
  if (ret == LS_COMPONENT_CONNECT_ALREADYEXISTS) {
    std::cerr << "Component : already exist !\n";
    return ret;
  }
  if (ret == LS_COMPONENT_CONNECT_BADCOMPONENTCONFIGURATOR) {
    std::cerr << "Component : bad component configurator !\n";
    return ret;
  }
  if (ret == LS_COMPONENT_CONNECT_INTERNALERROR) {
    std::cerr << "Component : cannot connect !\n";
    return ret;
  }

  char* s;
  s = logComponentBase->getName();
  std::cout << "name:" << s << "\n";
  delete s;
  s = logComponentBase->getHostname();
  std::cout << "hostname:" << s << "\n";
  delete s;

  ret = logComponentBase->connect("test connect 2");
  if (ret == LS_COMPONENT_CONNECT_BADNAME) {
    std::cerr << "Component : bad name !\n";
    return ret;
  }
  if (ret == LS_COMPONENT_CONNECT_ALREADYEXISTS) {
    std::cerr << "Component : already exists !\n";
    return ret;
  }
  if (ret == LS_COMPONENT_CONNECT_BADCOMPONENTCONFIGURATOR) {
    std::cerr << "Component : bad component configurator !\n";
    return ret;
  }
  if (ret == LS_COMPONENT_CONNECT_INTERNALERROR) {
    std::cerr << "Component : cannot connect !\n";
    return ret;
  }

  logComponentBase->sendMsg("TEST", "test message");
  sleep(1);  // make sure the message is sent

  ret = logComponentBase->disconnect("test disconnection of component");
  if (ret == LS_COMPONENT_DISCONNECT_NOTEXISTS) {
    std::cerr << "Component : cannot disconnect, not exists !\n";
    return ret;
  }

  //  logComponentBase->setName("testComponent_C++");
  ret = logComponentBase->connect("test connect 3");
  if (ret != LS_OK) {
    return ret;
  }

  logComponentBase->sendMsg("ANY", "another test message");
  sleep(1);  // make sure this message is sent

  std::cout << "'IN' tag wanted ? " << logComponentBase->isLog("IN") << "\n";
  std::cout << "'TEST' tag wanted ? " << logComponentBase->isLog("TAG") << "\n";
  ret = logComponentBase->disconnect("test disconnect 2");
  if (ret != LS_OK) {
    return ret;
  }

  delete logComponentBase;
  std::cout << "All tests passed successfully !\n";
  return ret;
}

