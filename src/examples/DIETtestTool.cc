/****************************************************************************/
/* A test for the liblogtoolbase.a                                          */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.7  2011/05/13 08:17:52  bdepardo
 * Update ORB manager with changes made in DIET ORB manager.
 *
 * Revision 1.6  2011/04/22 11:44:22  bdepardo
 * Use a signal handler to handle background option.
 * This handler catches SIGINT and SIGTERM.
 *
 * Revision 1.5  2011/03/03 14:56:35  bdepardo
 * Fixed compilation warning
 *
 * Revision 1.4  2011/02/04 15:25:22  bdepardo
 * Removed unused variables.
 * Removed resource leak.
 *
 * Revision 1.3  2010/12/24 09:47:07  kcoulomb
 * Remove deprecated tools
 * tool gets generated name
 *
 * Revision 1.2  2010/12/03 12:40:26  kcoulomb
 * MAJ log to use forwarders
 *
 * Revision 1.1  2010/11/10 05:59:44  kcoulomb
 * Add a tool for DIET, the generated files can be used with VizDIET
 *
 * Revision 1.11  2010/11/10 04:32:51  kcoulomb
 * Fix the rebind problem
 *
 * Revision 1.10  2010/11/10 02:27:43  kcoulomb
 * Update the log to use the forwarder.
 * Programm run without launching forwarders but fails with forwarder.
 *
 * Revision 1.9  2008/11/04 08:21:05  bdepardo
 * Added #include <stdlib.h>.
 * Now compiles with gcc 4.3.2
 *
 * Revision 1.8  2005/07/01 12:55:01  rbolze
 * message recieve from LogCentral can be bigger now !
 *
 * Revision 1.7  2004/10/08 11:20:49  hdail
 * Corrected problem seen under 64-bit opteron: can not cast CORBA Long to
 * time_t directly.
 *
 * Revision 1.6  2004/07/08 20:32:15  rbolze
 * make som changes :
 *  - create a tool named DIETLogTool that is specific for DIET and VizDIET
 *  - create README to explain what is DIETLogTool
 *  - modify testTool to be an example independant from DIET
 *  - modify Makefile.am to take account of all this changes
 *
 * Revision 1.5  2004/05/25 12:02:39  hdail
 * Sleep isn't found on some systems with current inclusions.  Added
 * inclusion of unistd.h when HAVE_BACKGROUND is true.
 *
 * Revision 1.4  2004/05/12 12:39:04  hdail
 * Add support for running services in the background (at a loss of clean
 * exit with interactive 'Q').  Behavior can be controlled via configure.
 *
 * Revision 1.3  2004/03/16 17:57:22  rbolze
 * Now  you can tell to write log messages into a specific filname
 * the default filename is Dietlog.log
 * the command line to launch testTool should be >./testTool <filname>
 *
 * Revision 1.2  2004/03/02 08:41:21  rbolze
 * print message in VizDiet readable format, save in a file named logtest.log
 *
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/


#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>

#include "LogToolBase.hh"
#include "LogORBMgr.hh"
#include "LogCentralTool_impl.hh"

class MyMsgRecv : public POA_ToolMsgReceiver,
                  public PortableServer::RefCountServantBase {
public:
  LogCentralTool_ptr myLCT;
  char* name;
  std::string filename;

  filter_t filter;

  explicit MyMsgRecv(const char* name) {
    this->name = CORBA::string_dup(name);

    filter.filterName = CORBA::string_dup("allFilter");
    filter.tagList.length(1);
    filter.tagList[0] = CORBA::string_dup("*");
    filter.componentList.length(1);
    filter.componentList[0] = CORBA::string_dup("*");
    filename = "DIETLogTool.log";
  }

  int
  disconnect() {
    return myLCT->disconnectTool (name);
  }

  void
  run() {
    CORBA::Object_ptr myLCTptr;

    // Connexion to the LCT to get messages
    myLCT =
      LogORBMgr::getMgr()->resolve<LogCentralTool,
                                   LogCentralTool_ptr>("LogServiceT", "LCT");
    if (CORBA::is_nil(myLCT)) {
      fprintf(stderr, "Failed to narrow the LCT ! \n");
    }

    try {
      LogORBMgr::getMgr()->bind("LogServiceT", name, _this(), true);
      LogORBMgr::getMgr()->fwdsBind("LogServiceT", name,
                                    LogORBMgr::getMgr()->getIOR(_this()));
    }
    catch (...) {
      fprintf(stderr, "Bind failed  in the LogService context\n");
    }

    try {
      myLCT->connectTool(name, name);
      myLCT->addFilter(name, filter);
    }
    catch (...) {
      fprintf(stderr, "Caught an unknown exception\n");
    }
  }

  void
  sendMsg(const log_msg_buf_t& msg) {
    struct tm *m;
    for (CORBA::ULong i = 0; i < msg.length(); i++) {
      long int tempSec = msg[i].time.sec;
      m = gmtime(&tempSec);
      char s[256];
      std::string log;
      // componentName
      log.append(msg[i].componentName);
      log.append(":");
      // Date in the right format
      sprintf(s, "%02d:%02d:%02d:%02d:%02d:%02d:%d:",
              m->tm_mday, m->tm_mon + 1, m->tm_year + 1900, m->tm_hour,
              m->tm_min, m->tm_sec, (int)msg[i].time.msec);
      log.append(s);
      // Channel : LOGCENTRAL
      log.append("LOGCENTRAL:");
      // priority : 5 not important
      log.append("5:");
      // tagtype
      log.append(msg[i].tag);
      log.append(":");
      // msg
      log.append(msg[i].msg);
      std::ofstream dest(filename.c_str(), std::ios::app);
      dest << log << "\n";
      dest.close();
      std::cout << log << "\n";
    }
  }

  void
  setFilter(char* description_file_name) {
    if (description_file_name == NULL) {
      return;
    }
    char** tags = NULL;
    FILE* filter_file;
    printf("filter_file_name = %s\n", description_file_name);
    filter_file = fopen(description_file_name, "r");
    int nb = 0;
    if (filter_file != NULL) {
      char line[BUFSIZ];
      while (fgets(line, BUFSIZ, filter_file) != NULL) {
        if ((line[0] != '#') & (line[0] != '\n') & (line[0] != ' ')) {
          nb++;
          char tag[BUFSIZ];
          sscanf(line, "%s", tag);
          tags = (char**)realloc(tags, (nb)*sizeof(char*));
          tags[nb-1] = (char*)malloc(strlen(line)*sizeof(char));
          sprintf(tags[nb-1], "%s", tag);
        }
      }
      fclose(filter_file);
    }
    filter.filterName = CORBA::string_dup("myfilter");
    filter.componentList.length(1);
    filter.componentList[0] = CORBA::string_dup("*");
    filter.tagList.length(nb);
    for (int i = 0; i < nb; i++) {
      printf("add_tag : %s\n", tags[i]);
      filter.tagList[i] = CORBA::string_dup(tags[i]);
    }
  }

  void
  setFilename(char* name) {
    if (name) {
      filename = name;
    }
  }
};


int
main(int argc, char** argv) {
  try {
    LogORBMgr::init(argc, argv);
  } catch (...) {
    fprintf(stderr, "ORB initialization failed");
  }

  MyMsgRecv* myLTB;
  char name[200];
  char host[50];
  srand(time(NULL));
  if (gethostname(host, 50)) {
    fprintf(stderr, "Failed to build the tool name. Leaving \n");
  }
  snprintf(name, 200, "dietTool_%s_%d", host, rand());
  myLTB = new MyMsgRecv(name);
  if (argc >= 2) {
    myLTB->setFilename(argv[1]);
  }
  if (argc >= 3) {
    myLTB->setFilter(argv[2]);
  }
  LogORBMgr::getMgr()->activate((MyMsgRecv*)myLTB);
  myLTB->run();


  /* The code to support interactive quit via user input of q or Q is
   * incompatible with backgrounding the LogCentral process.  Use
   * configure option --enable-background to select which you prefer.
   */
  try {
    LogORBMgr::getMgr()->wait();
  } catch (...) {
    std::cerr << "Error while exiting the ORBMgr::wait() function\n";
  }
}
