/****************************************************************************/
/* Log forwarder implementation                                             */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Gael Le Mahec   (gael.le.mahec@ens-lyon.fr)                         */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/

#include "LogForwarder.hh"

#include <cstring>
#include <list>
#include <iostream>
#include <stdexcept>
#include <string>
#include <unistd.h>  // For gethostname()

#include "CorbaLogForwarder.hh"
#include "LogORBMgr.hh"
#include "commonLogTypes.hh"

#include "monitor/LogCentralComponent_impl.hh"
#include "monitor/LogCentralTool_impl.hh"

#include "debug.hh"

#ifdef MAXHOSTNAMELEN
#define MAX_HOSTNAME_LENGTH MAXHOSTNAMELEN
#else
#define MAX_HOSTNAME_LENGTH  255
#endif


bool
LogForwarder::remoteCall(std::string& objName) {
  if (objName.find("remote:") != 0) {
    /* Local network call: need to be forwarded to
     * the peer forwarder. Add the prefix.
     */
    objName = "remote:" + objName;
    return false;
  }
  /* Remote network call. Remove the prefix. */
  objName = objName.substr(strlen("remote:"));
  return true;
}


::CORBA::Object_ptr
LogForwarder::getObjectCache(const std::string& name) {
  std::map<std::string, ::CORBA::Object_ptr>::iterator it;

  cachesMutex.lock();
  if ((it = objectCache.find(name)) != objectCache.end()) {
    cachesMutex.unlock();
    return CORBA::Object::_duplicate(it->second);
  }
  cachesMutex.unlock();
  return ::CORBA::Object::_nil();
}

LogForwarder::~LogForwarder() {
}

LogForwarder::LogForwarder(const std::string& name) {
  char buffer[MAX_HOSTNAME_LENGTH+1];
  gethostname(buffer, MAX_HOSTNAME_LENGTH);

  this->name = name;
  this->host = buffer;

  // Wait for the peer init. The unlock will be done on setPeer().
  peerMutex.lock();
}


LogCentralComponent_ptr
LogForwarder::getLogCentralComponent(const char* name) {
  std::string nm(name);
  ::CORBA::Object_ptr object;

  if (nm.find('/') == std::string::npos) {
    nm = std::string(LOGCOMPCTXT)+"/"+nm;
  }
  object = getObjectCache(nm);
  if (!CORBA::is_nil(object)) {
    if (object->_non_existent()) {
      removeObjectFromCache(name);
    } else {
      return LogCentralComponent::_duplicate(
        LogCentralComponent::_narrow(object));
    }
  }

  LogCentralComponentFwdrImpl * comp =
    new LogCentralComponentFwdrImpl(this->_this(), nm.c_str());

  LogORBMgr::getMgr()->activate(comp);

  cachesMutex.lock();
  servants[nm] = comp;
  objectCache[name] = CORBA::Object::_duplicate(comp->_this());
  cachesMutex.unlock();

  return LogCentralComponent::_duplicate(comp->_this());
}

LogCentralTool_ptr
LogForwarder::getLogCentralTool(const char* name) {
  std::string nm(name);
  ::CORBA::Object_ptr object;
  if (nm.find('/') == std::string::npos) {
    nm = std::string(LOGTOOLCTXT)+"/"+nm;
  }
  object = getObjectCache(nm);
  if (!CORBA::is_nil(object)) {
    if (object->_non_existent()) {
      removeObjectFromCache(name);
    } else {
      return LogCentralTool::_duplicate(LogCentralTool::_narrow(object));
    }
  }

  LogCentralToolFwdr_impl * comp =
    new LogCentralToolFwdr_impl(this->_this(), nm.c_str());

  LogORBMgr::getMgr()->activate(comp);

  cachesMutex.lock();
  servants[nm] = comp;
  objectCache[name] = CORBA::Object::_duplicate(comp->_this());
  cachesMutex.unlock();
  return LogCentralTool::_duplicate(comp->_this());
}

ComponentConfigurator_ptr
LogForwarder::getCompoConf(const char* name) {
  std::string nm(name);
  ::CORBA::Object_ptr object;

  if (nm.find('/') == std::string::npos) {
    nm = std::string(LOGCOMPCTXT)+"/"+nm;
  }
  object = getObjectCache(nm);
  if (!CORBA::is_nil(object)) {
    if (object->_non_existent()) {
      removeObjectFromCache(name);
    } else {
      return ComponentConfigurator::_duplicate(
        ComponentConfigurator::_narrow(object));
    }
  }

  ComponentConfiguratorFwdr_impl * comp =
    new ComponentConfiguratorFwdr_impl(this->_this(), nm.c_str());

  LogORBMgr::getMgr()->activate(comp);

  cachesMutex.lock();
  servants[nm] = comp;
  objectCache[name] = CORBA::Object::_duplicate(comp->_this());
  cachesMutex.unlock();

  return ComponentConfigurator::_duplicate(comp->_this());
}

ToolMsgReceiver_ptr
LogForwarder::getToolMsgReceiver(const char* name) {
  std::string nm(name);
  ::CORBA::Object_ptr object;
  if (nm.find('/') == std::string::npos) {
    nm = std::string(LOGTOOLCTXT)+"/"+nm;
  }
  object = getObjectCache(nm);
  if (!CORBA::is_nil(object)) {
    if (object->_non_existent()) {
      removeObjectFromCache(name);
    } else {
      return ToolMsgReceiver::_duplicate(ToolMsgReceiver::_narrow(object));
    }
  }

  ToolMsgReceiverFwdr_impl * comp =
    new ToolMsgReceiverFwdr_impl(this->_this(), nm.c_str());

  LogORBMgr::getMgr()->activate(comp);

  cachesMutex.lock();
  servants[nm] = comp;
  objectCache[name] = CORBA::Object::_duplicate(comp->_this());
  cachesMutex.unlock();
  return ToolMsgReceiver::_duplicate(comp->_this());
}


/* Common methods implementations. */
void
LogForwarder::ping(const char* compoName, const char* objName) {
  std::string objString(objName);
  std::string name;
  std::string ctxt;

  if (!remoteCall(objString)) {
    return getPeer()->ping(compoName, objString.c_str());
  }

  name = getName(objString);
  ctxt = getCtxt(objString);

  if (ctxt == std::string(LOGCOMPCTXT)) {
    LogCentralComponent_var logCentral = LogORBMgr::getMgr()->resolve
      <LogCentralComponent, LogCentralComponent_var>(LOGCOMPCTXT,
                                                     name,
                                                     this->name);
    return logCentral->ping(compoName);
  }
  if (ctxt == std::string(LOGTOOLCTXT)) {
    return;
  }

  throw LogBadNameException(objString.c_str(), __FUNCTION__, getName());
}


void
LogForwarder::bind(const char* objName, const char* ior) {
  /* To avoid crashes when the peer forwarder is not ready: */
  /* If the peer was not initialized, the following call is blocking. */
  peerMutex.lock();
  peerMutex.unlock();

  std::string objString(objName);
  std::string name;
  std::string ctxt;

  if (!remoteCall(objString)) {
    TRACE_TEXT(TRACE_MAIN_STEPS, "Forward bind to peer\n");
    return getPeer()->bind(objString.c_str(), ior);
  }
  ctxt = getCtxt(objString);
  name = getName(objString);
  TRACE_TEXT(TRACE_MAIN_STEPS, "Bind locally\n");

  /* NEW: Tag the object with the forwarder name. */
  std::string newIOR =
    LogORBMgr::convertIOR(ior, std::string("@") + getName(), 0);

  LogORBMgr::getMgr()->bind(ctxt, name, newIOR, true);
  // Broadcast the binding to all forwarders.
  LogORBMgr::getMgr()->fwdsBind(ctxt, name, newIOR, this->name);
  TRACE_TEXT(TRACE_MAIN_STEPS, "Binded! (" << ctxt << "/"
             << name << ")\n");
}

/* Return the local bindings. Result is a set of couple
 * (object name, object ior)
 */
LogSeqString*
LogForwarder::getBindings(const char* ctxt) {
  std::list<std::string> objects;
  std::list<std::string>::iterator it;
  LogSeqString* result = new LogSeqString();
  unsigned int cmpt = 0;

  objects = LogORBMgr::getMgr()->list(ctxt);
  result->length(objects.size()*2);

  for (it = objects.begin(); it != objects.end(); ++it) {
    try {
      CORBA::Object_ptr obj =
        LogORBMgr::getMgr()->resolveObject(ctxt, it->c_str(), "no-Forwarder");
      (*result)[cmpt++] = it->c_str();
      (*result)[cmpt++] = LogORBMgr::getMgr()->getIOR(obj).c_str();
    } catch (const std::runtime_error& err) {
      continue;
    }
  }
  result->length(cmpt);
  return result;
}

void
LogForwarder::unbind(const char* objName) {
  std::string objString(objName);
  std::string name;
  std::string ctxt;

  if (!remoteCall(objString)) {
    return getPeer()->unbind(objString.c_str());
  }

  name = objString;

  if (name.find('/') == std::string::npos) {
    return;
  }

  ctxt = getCtxt(objString);
  name = getName(objString);

  removeObjectFromCache(name);

  LogORBMgr::getMgr()->unbind(ctxt, name);
  // Broadcast the unbinding to all forwarders.
  LogORBMgr::getMgr()->fwdsUnbind(ctxt, name, this->name);
}

void
LogForwarder::removeObjectFromCache(const std::string& name) {
  std::map<std::string, ::CORBA::Object_ptr>::iterator it;
  std::map<std::string, PortableServer::ServantBase*>::iterator jt;
  /* If the object is in the servant cache. */
  cachesMutex.lock();
  if ((jt = servants.find(name)) != servants.end()) {
    /* - Deactivate object. */
    try {
      LogORBMgr::getMgr()->deactivate(jt->second);
    } catch (...) {
      /* There was a problem with the servant. But we want
       * to remove it...
       */
    }
    /* - Remove activated servants. */
    servants.erase(jt);
  }
  /* Remove the object from the cache. */
  if ((it = objectCache.find(name)) != objectCache.end()) {
    objectCache.erase(it);
  }

  cachesMutex.unlock();
}

/* Remove non existing objects from the caches. */
void
LogForwarder::cleanCaches() {
  std::map<std::string, ::CORBA::Object_ptr>::iterator it;
  std::list<std::string> invalidObjects;
  std::list<std::string>::const_iterator jt;

  cachesMutex.lock();
  for (it = objectCache.begin(); it != objectCache.end(); ++it) {
    try {
      CorbaLogForwarder_var object = CorbaLogForwarder::_narrow(it->second);
      object->getName();
    } catch (const CORBA::TRANSIENT& err) {
      invalidObjects.push_back(it->first);
    }
  }
  cachesMutex.unlock();
  for (jt = invalidObjects.begin(); jt != invalidObjects.end(); ++jt) {
    removeObjectFromCache(*jt);
  }
}

void
LogForwarder::connectPeer(const char* ior, const char* host,
                          const ::CORBA::Long port) {
  std::string converted = LogORBMgr::convertIOR(ior, host, port);
  setPeer(LogORBMgr::getMgr()->resolve<CorbaLogForwarder,
                                       CorbaLogForwarder_ptr>(converted));
}

void
LogForwarder::setPeer(CorbaLogForwarder_ptr peer) {
  this->peer = CorbaLogForwarder::_duplicate(CorbaLogForwarder::_narrow(peer));
  // Peer was init. The lock was done on the constructor.
  peerMutex.unlock();
}

CorbaLogForwarder_var
LogForwarder::getPeer() {
  // Wait for setPeer
  peerMutex.lock();
  peerMutex.unlock();
  try {
    // Check if peer is still alive
    peer->getName();
  } catch (const CORBA::COMM_FAILURE& err) {
    // Lock peerMutex, then wait for setPeer
    // (setPeer() unlock the mutex
    peerMutex.lock();
    peerMutex.lock();
    peerMutex.unlock();
  } catch (const CORBA::TRANSIENT& err) {
    // Lock peerMutex, then wait for setPeer
    // (setPeer() unlock the mutex
    peerMutex.lock();
    peerMutex.lock();
    peerMutex.unlock();
  }
  return peer;
}


char*
LogForwarder::getIOR() {
  return CORBA::string_dup(LogORBMgr::getMgr()->getIOR(_this()).c_str());
}

char*
LogForwarder::getName() {
  return CORBA::string_dup(name.c_str());
}

char*
LogForwarder::getPeerName() {
  return CORBA::string_dup(getPeer()->getName());
}

char*
LogForwarder::getHost() {
  return CORBA::string_dup(host.c_str());
}

char*
LogForwarder::getPeerHost() {
  return CORBA::string_dup(getPeer()->getHost());
}


LogSeqString*
LogForwarder::routeTree() {
  LogSeqString* result = new LogSeqString();
  return result;
}

std::string
LogForwarder::getName(const std::string& namectxt) {
  size_t pos = namectxt.find('/');
  if (pos == std::string::npos) {
    return namectxt;
  }

  return namectxt.substr(pos+1);
}

std::string
LogForwarder::getCtxt(const std::string& namectxt) {
  size_t pos = namectxt.find('/');
  if (pos == std::string::npos) {
    return "";
  }

  return namectxt.substr(0, pos);
}



void
LogForwarder::setTagFilter(const ::tag_list_t& tagList,
                           const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->setTagFilter(tagList, objString.c_str());
  }

  name = getName(objString);

  ComponentConfigurator_var cfg =
    LogORBMgr::getMgr()->resolve<ComponentConfigurator,
                                 ComponentConfigurator_var>(LOGCOMPCTXT,
                                                            name,
                                                            this->name);
  return cfg->setTagFilter(tagList);
}

void
LogForwarder::addTagFilter(const ::tag_list_t& tagList,
                           const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->addTagFilter(tagList, objString.c_str());
  }

  name = getName(objString);

  ComponentConfigurator_var cfg =
    LogORBMgr::getMgr()->resolve<ComponentConfigurator,
                                 ComponentConfigurator_var>(LOGCOMPCTXT,
                                                            name,
                                                            this->name);
  return cfg->addTagFilter(tagList);
}

void
LogForwarder::removeTagFilter(const ::tag_list_t& tagList,
                              const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->removeTagFilter(tagList, objString.c_str());
  }

  name = getName(objString);

  ComponentConfigurator_var cfg =
    LogORBMgr::getMgr()->resolve<ComponentConfigurator,
                                 ComponentConfigurator_var>(LOGCOMPCTXT,
                                                            name,
                                                            this->name);
  return cfg->removeTagFilter(tagList);
}

void
LogForwarder::test(const char* objName) {
  std::string objString(objName);
  std::string name;

  name = getName(objString);

  ComponentConfigurator_var cfg =
    LogORBMgr::getMgr()->resolve<ComponentConfigurator,
                                 ComponentConfigurator_var>(LOGCOMPCTXT,
                                                            name,
                                                            this->name);
  return cfg->test();
}



void
LogForwarder::sendMsg(const log_msg_buf_t& msgBuf, const char*  objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->sendMsg(msgBuf, objString.c_str());
  }

  name = getName(objString);

  ToolMsgReceiver_var cfg =
    LogORBMgr::getMgr()->resolve<ToolMsgReceiver,
                                 ToolMsgReceiver_var>(LOGTOOLCTXT,
                                                      name,
                                                      this->name);
  return cfg->sendMsg(msgBuf);
}

/**
 * Connect a Tool with its toolName, which must be unique among all
 * tools. The return value indicates the success of the connection.
 * If ALREADYEXISTS is returned, the tool could not be attached, as
 * the specified toolName already exists. In this case the tool must
 * reconnect with another name before specifying any filters. If the
 * tool sends an empty toolName, the LogCentral will provide a unique
 * toolName and pass it back to the tool.
 */
short
LogForwarder::connectTool(char*& toolName,
                          const char* msgReceiver,
                          const char* objName) {
  std::string objString(objName);
  std::string name;
  if (!remoteCall(objString)) {
    getPeer()->connectTool(toolName, msgReceiver, objString.c_str());
    return 1;
  }
  name = getName(objString);

  LogCentralTool_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralTool,
                                 LogCentralTool_var>(LOGTOOLCTXT,
                                                     name,
                                                     this->name);
  return cfg->connectTool(toolName, msgReceiver);
}

/**
 * Disconnects a connected tool from the monitor. No further
 * filterconfigurations should be sent after this call. The
 * toolMsgReceiver will not be used by the monitor any more
 * after this call. Returns NOTCONNECTED if the calling tool
 * was not connected.
 */
short
LogForwarder::disconnectTool(const char* toolName, const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->disconnectTool(toolName, objString.c_str());
  }

  name = getName(objString);

  LogCentralTool_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralTool,
                                 LogCentralTool_var>(LOGTOOLCTXT,
                                                     name,
                                                     this->name);
  return cfg->disconnectTool(toolName);
}

/**
 * Returns a list of possible tags. This is just a convenience
 * functions and returns the values that are specified in a
 * configuration file. If the file is not up to date, the
 * application may generate more tags than defined in this
 * list.
 */
tag_list_t*
LogForwarder::getDefinedTags(const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->getDefinedTags(objString.c_str());
  }

  name = getName(objString);

  LogCentralTool_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralTool,
                                 LogCentralTool_var>(LOGTOOLCTXT,
                                                     name,
                                                     this->name);
  return cfg->getDefinedTags();
}

/**
 * Returns a list of actually connected Components. This is just
 * a convenience function, as the whole state of the system will
 * be sent to the tool right after connection (in the form of
 * messages)
 */
component_list_t*
LogForwarder::getDefinedComponents(const char*  objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->getDefinedComponents(objString.c_str());
  }

  name = getName(objString);

  LogCentralTool_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralTool,
                                 LogCentralTool_var>(LOGCOMPCTXT,
                                                     name,
                                                     this->name);
  return cfg->getDefinedComponents();
}

/**
 * Create a filter for this tool on the monitor. Messages matching
 * this filter will be forwarded to the tool. The filter will be
 * identified by its name, which is a part of filter_t. A tool
 * can have as much filters as it wants. Returns ALREADYEXISTS if
 * another filter with this name is already registered.
 */
short
LogForwarder::addFilter(const char* toolName,
                        const filter_t& filter,
                        const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->addFilter(toolName, filter, objString.c_str());
  }

  name = getName(objString);

  LogCentralTool_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralTool,
                                 LogCentralTool_var>(LOGTOOLCTXT,
                                                     name,
                                                     this->name);
  return cfg->addFilter(toolName, filter);
}

/**
 * Removes a existing filter from the tools filterList. The filter
 * will be identified by its name in the filter_t. If the specified
 * filter does not exist, NOTEXISTS is returned.
 */
short
LogForwarder::removeFilter(const char* toolName,
                           const char* filterName,
                           const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->removeFilter(toolName, filterName, objString.c_str());
  }

  name = getName(objString);

  LogCentralTool_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralTool,
                                 LogCentralTool_var>(LOGTOOLCTXT,
                                                     name,
                                                     this->name);
  return cfg->removeFilter(toolName, filterName);
}

/**
 * Removes all defined filters.
 */
short
LogForwarder::flushAllFilters(const char* toolName, const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->flushAllFilters(toolName, objString.c_str());
  }

  name = getName(objString);

  LogCentralTool_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralTool,
                                 LogCentralTool_var>(LOGTOOLCTXT,
                                                     name,
                                                     this->name);
  return cfg->flushAllFilters(toolName);
}


short
LogForwarder::connectComponent(char*& componentName,
                               const char* componentHostname,
                               const char* message,
                               const char* compConfigurator,
                               const log_time_t& componentTime,
                               tag_list_t& initialConfig,
                               const char* objName) {
  std::string objString(objName);
  std::string name;
  if (!remoteCall(objString)) {
    return getPeer()->connectComponent(componentName,
                                       componentHostname,
                                       message,
                                       compConfigurator,
                                       componentTime,
                                       initialConfig,
                                       objString.c_str());
  }
  name = getName(objString);

  LogCentralComponent_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralComponent,
                                 LogCentralComponent_var>(LOGCOMPCTXT,
                                                          name,
                                                          this->name);
  return cfg->connectComponent(componentName,
                               componentHostname,
                               message,
                               compConfigurator,
                               componentTime,
                               initialConfig);
}



short
LogForwarder::disconnectComponent(const char* componentName,
                                  const char* message,
                                  const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->disconnectComponent(componentName,
                                          message,
                                          objString.c_str());
  }

  name = getName(objString);

  LogCentralComponent_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralComponent,
                                 LogCentralComponent_var>(LOGCOMPCTXT,
                                                          name,
                                                          this->name);
  return cfg->disconnectComponent(componentName, message);
}




void
LogForwarder::sendBuffer(const log_msg_buf_t &buffer,
                         const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->sendBuffer(buffer,
                                 objString.c_str());
  }

  name = getName(objString);

  LogCentralComponent_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralComponent,
                                 LogCentralComponent_var>(LOGCOMPCTXT,
                                                          name,
                                                          this->name);
  return cfg->sendBuffer(buffer);
}


void
LogForwarder::synchronize(const char* componentName,
                          const log_time_t& componentTime,
                          const char* objName) {
  std::string objString(objName);
  std::string name;

  if (!remoteCall(objString)) {
    return getPeer()->synchronize(componentName,
                                  componentTime,
                                  objString.c_str());
  }

  name = getName(objString);

  LogCentralComponent_var cfg =
    LogORBMgr::getMgr()->resolve<LogCentralComponent,
                                 LogCentralComponent_var>(LOGCOMPCTXT,
                                                          name,
                                                          this->name);
  return cfg->synchronize(componentName, componentTime);
}

std::list<std::string>
LogForwarder::otherForwarders() const {
  LogORBMgr* mgr = LogORBMgr::getMgr();
  std::list<std::string> result = mgr->list(LOGFWRDCTXT);

  result.remove(name);
  return result;
}

