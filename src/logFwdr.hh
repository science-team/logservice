/****************************************************************************/
/* Log forwarder implementation - Forwarder executable                      */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Gael Le Mahec   (gael.le.mahec@ens-lyon.fr)                         */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/

#ifndef _LOGFWDR_HH_
#define _LOGFWDR_HH_

#include <string>
#include "Options.hh"

class LogForwarder;
class LogORBMgr;


class FwrdConfig : public Configuration {
public:
  explicit FwrdConfig(const std::string& pgName);

  const std::string&
  getName() const;

  const std::string&
  getPeerName() const;

  const std::string&
  getPeerIOR() const;

  const std::string&
  getSshHost() const;

  const std::string&
  getRemoteHost() const;

  const std::string&
  getRemotePortTo() const;

  const std::string&
  getRemotePortFrom() const;

  const std::string&
  getLocalPortFrom() const;

  bool
  createTo() const;

  bool
  createFrom() const;

  const std::string&
  getSshPath() const;

  const std::string&
  getSshPort() const;

  const std::string&
  getSshLogin() const;

  const std::string&
  getSshKeyPath() const;

  int
  getNbRetry() const;

  unsigned int
  getWaitingTime() const;

  const std::string&
  getCfgPath() const;

  void
  setName(const std::string& name);

  void
  setPeerName(const std::string& name);

  void
  setPeerIOR(const std::string& ior);

  void
  setSshHost(const std::string& host);

  void
  setRemoteHost(const std::string& host);

  void
  setRemotePortTo(const std::string& port);

  void
  setRemotePortFrom(const std::string& port);

  void
  setLocalPortFrom(const std::string& port);

  void
  createTo(bool create);

  void
  createFrom(bool create);

  void
  setSshPath(const std::string& path);

  void
  setSshPort(const std::string& port);

  void
  setSshLogin(const std::string& login);

  void
  setSshKeyPath(const std::string& path);

  void
  setNbRetry(const int nb);

  void
  setWaitingTime(const unsigned int time);

  void
  setCfgPath(const std::string& path);

private:
  std::string name;
  std::string peerName;
  std::string peerHost;
  std::string peerPort;
  std::string peerIOR;

  std::string sshHost;
  std::string remoteHost;
  std::string localPortFrom;
  std::string remotePortTo;
  std::string remotePortFrom;
  bool createTunnelTo;
  bool createTunnelFrom;
  std::string sshPath;
  std::string sshPort;
  std::string sshLogin;
  std::string sshKeyPath;
  int nbRetry;
  std::string cfgPath;
  unsigned int waitingTime;
};

int
connectPeer(const std::string &ior, const std::string &peerIOR,
            const std::string &newHost, const std::string &remoteHost,
            int localPortFrom, int remotePortFrom,
            LogForwarder *forwarder, LogORBMgr* mgr);

void
name(const std::string& name, Configuration* cfg);

void
peer_name(const std::string& name, Configuration* cfg);

void
peer_ior(const std::string& ior, Configuration* cfg);

void
ssh_host(const std::string& host, Configuration* cfg);

void
remote_host(const std::string& host, Configuration* cfg);

void
remote_port_to(const std::string& port, Configuration* cfg);

void
remote_port_from(const std::string& port, Configuration* cfg);

void
local_port_from(const std::string& port, Configuration* cfg);

void
create(const std::string& create, Configuration* cfg);

void
ssh_path(const std::string& path, Configuration* cfg);

void
ssh_port(const std::string& port, Configuration* cfg);

void
ssh_login(const std::string& login, Configuration* cfg);

void
key_path(const std::string& path, Configuration* cfg);

void
nb_retry(const std::string& nb, Configuration* cfg);

void
tunnel_wait(const std::string& time, Configuration* cfg);

/* Transformation function for the host name. */
int
change(int c);

#endif  // _LOGFWDR_HH_
