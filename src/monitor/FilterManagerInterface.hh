/****************************************************************************/
/* interface that defines functions that an implementation of a             */
/* FilterManager must offer to be notified correctly on systemchanges       */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#ifndef _FILTERMANAGERINTERFACE_HH_
#define _FILTERMANAGERINTERFACE_HH_

#include "LogTypes.hh"
#include "ToolList.hh"
#include "ComponentList.hh"

/**
 * Interface to the Filtermanager (Abstract class)
 * The FilterManager keeps track of all attached Components
 * and of all configured filters. Based on this information,
 * it can be used to filter incoming Messages and forward
 * them to the appropriate tools. If tools change their
 * filters, the FilterManager adjusts the Components output.
 */
class FilterManagerInterface {
public:
  virtual ~FilterManagerInterface() {
  }

  /**
   * Notify the manager that a new tool has connected.
   * The Tool must already exist in the toolList. iter must
   * be an ReadIterator to the toolList. It should be the
   * same iterator that was used for inserting the tool.
   */
  virtual void
  toolConnect(const char* toolName, ToolList::ReadIterator* iter) = 0;

  /**
   * Notify the manager that a tool is about disconnects.
   * This function must be called right before deleting the
   * tool from the list, as Filtermanager will delete all its
   * internal data on this tool (including remaining filters).
   * The iterator passed should be the iterator used for
   * deleting the tool.
   */
  virtual void
  toolDisconnect(const char* toolName, ToolList::ReadIterator* iter) = 0;

  /**
   * Notify the manager that a new filter has been added.
   * the filter must already exist in the toollist. A
   * iterator used for inserting should be passed.
   */
  virtual void
  addFilter(const char* toolName, const char* filterName,
            ToolList::ReadIterator* iter) = 0;

  /**
   * Notify the manager that a filter will be removed.
   * This function should be called right before deleting
   * the filter. The iterator for deleting the filter should
   * be passed.
   */
  virtual void
  removeFilter(const char* toolName, const char* filterName,
               ToolList::ReadIterator* iter) = 0;

  /**
   * Notify the manager that all filters for a certain
   * tool will be removed. The filtermanager might implement
   * this more efficient that removing each filter one by
   * one.
   */
  virtual void
  flushAllFilters(const char* toolName, ToolList::ReadIterator* iter) = 0;

  /**
   * Notify the manager that a component connected.
   * The component should be already in the list, the
   * corresponding iterator should be passed.
   */
  virtual tag_list_t*
  componentConnect(const char* componentName,
                   ComponentList::ReadIterator* iter) = 0;

  /**
   * Notify the manager that a component will disconnects.
   * This must happen right before the deletion of the
   * component. The iterator passed should be the same
   * iterator used for deleting.
   */
  virtual void
  componentDisconnect(const char* componentName,
                      ComponentList::ReadIterator* iter) = 0;

  /**
   * Forwards a message to all interested Tools.
   * The message will be checked and stored in the outBuffer
   * of all tools with a matching filter.
   */
  virtual void
  sendMessageWithFilters(log_msg_t* message) = 0;
};

#endif
