/****************************************************************************/
/* The SimpleFilterManager implements the FilterManagerInterface. This      */
/* Implementation is complete, but it is not very efficient.                */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ***************************************************************************/

#ifndef _SIMPLEFILTERMANAGER_HH_
#define _SIMPLEFILTERMANAGER_HH_

#include "LogTypes.hh"
#include "ToolList.hh"
#include "ComponentList.hh"
#include "FilterManagerInterface.hh"
#include "FullLinkedList.hh"

/**
 * Define an internal datastructure
 */
struct ConfigElement {
  CORBA::String_var componentName;
  tag_list_t config;
  tag_list_t oldConfig;
};

/**
 * A list to hold the configuration of components
 */
typedef FullLinkedList<ConfigElement> ConfigList;


/**
 * Simple implementation of the FilterManagerInterface
 * Mainly offers empty functions to make testers compile/run
 */
class SimpleFilterManager:public FilterManagerInterface {
public:
  /**
   * Create a SimpleFilterManager.
   *
   * @param toolList The ToolList to use
   * @param compList The ComponentList to use
   * @param stateTags A list of all Tags that are important
   *   for the system state and must always be sent. The
   *   FilterManager copies this list and does not take its
   *   ownership.
   */
  SimpleFilterManager(ToolList* toolList,
                      ComponentList* compList,
                      tag_list_t* stateTags);

  virtual
  ~SimpleFilterManager();

  /**
   * Notify the manager that a new tool has connected.
   * The Tool must already exist in the toolList. iter must
   * be an ReadIterator to the toolList. It should be the
   * same iterator that was used for inserting the tool.
   */
  void
  toolConnect(const char* toolName, ToolList::ReadIterator* iter);

  /**
   * Notify the manager that a tool is about to disconnect.
   * This function must be called right before deleting the
   * tool from the list, as Filtermanager will delete all its
   * internal data on this tool (including remaining filters).
   * The iterator passed should be the iterator used for
   * deleting the tool.
   */
  void
  toolDisconnect(const char* toolName, ToolList::ReadIterator* iter);

  /**
   * Notify the manager that a new filter has been added.
   * the filter must already exist in the toollist. A
   * iterator used for inserting should be passed.
   */
  void
  addFilter(const char* toolName, const char* filterName,
            ToolList::ReadIterator* iter);

  /**
   * Notify the manager that a filter will be removed.
   * This function should be called right before deleting
   * the filter. The iterator for deleting the filter should
   * be passed.
   */
  void
  removeFilter(const char* toolName, const char* filterName,
               ToolList::ReadIterator* iter);

  /**
   * Notify the manager that all filters for a certain
   * tool will be removed. The filtermanager might implement
   * this more efficient that removing each filter one by
   * one.
   */
  virtual void
  flushAllFilters(const char* toolName, ToolList::ReadIterator* iter);

  /**
   * Notify the manager that a component has connected
   * The component should be already in the list, the
   * corresponding iterator should be passed.
   */
  tag_list_t*
  componentConnect(const char* componentName,
                   ComponentList::ReadIterator* iter);

  /**
   * Notify the manager that a component will disconnect.
   * This must happen right before the deletion of the
   * component. The iterator passed should be the same
   * iterator used for deleting.
   */
  void
  componentDisconnect(const char* componentName,
                      ComponentList::ReadIterator* iter);

  /**
   * Forwards a message to all interested Tools.
   * The message will be checked and stored in the outBuffer
   * of all tools with a matching filter.
   */
  void
  sendMessageWithFilters(log_msg_t* message);

private:
  ToolList* toolList;
  ComponentList* componentList;

  /**
   * hold current configuration for each component
   */
  ConfigList configList;

  /**
   * taglist that contains stateChanging tags that must always be sent
   */
  tag_list_t systemStateTags;

  /**
   * Checks if a given component_list_t contains the
   * value given in name. list may contain the star
   */
  bool
  containsComponent(component_list_t* list, const char* name);

  /**
   * Checks if a given component_list_t contains the
   * value given in componentName. Stars ('*') are not treated
   * as special characters
   */
  bool
  containsComponentName(component_list_t* list, const char* name);

  /**
   * Checks if a given component_list_t contains the '*'
   */
  bool
  containsComponentStar(component_list_t* list);

  /**
   * Checks if a given tag_list_t contains the
   * value given in name. list may contain the star
   */
  bool
  containsTag(tag_list_t* list, const char* name);

  /**
   * Checks if a given tag_list_t contains the
   * value given in name. Stars ('*') are not treated
   * as special characters
   */
  bool
  containsTagName(tag_list_t* list, const char* name);

  /**
   * Checks if a given component_list_t contains the '*'
   */
  bool
  containsTagStar(tag_list_t* list);

  /**
   * Adds the tags in the srcList to the destList. If the tags
   * already exist in the destList, they are not copied. The
   * destList will not contain duplicates
   */
  void
  addTagList(tag_list_t* destList, tag_list_t* srcList);

  /**
   * Add a filter to the system. The tags of this filter will
   * be added to all applying components.
   */
  void
  addFilter(filter_t* filter);

  /**
   * Forward the configuration of the components (as stored in the
   * configList to the ComponentConfigurators.
   */
  void
  updateComponentConfigs();
};

#endif
