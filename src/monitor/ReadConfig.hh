/****************************************************************************/
/* Header of the ReadConfig class                                           */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ***************************************************************************/

#ifndef _READCONFIG_HH_
#define _READCONFIG_HH_

#include <cstdio>
#include "LogTypes.hh"

const short LS_PARSE_ALREADYPARSED = 1;
const short LS_PARSE_FILEERROR = 2;
const short LS_PARSE_SECTIONNOTFOUND = 3;

class ReadConfig {
public:
  ReadConfig(const char* configFilename, bool* success);

  ~ReadConfig();

  /**
   * Parse the config file.
   * @return an error level
   */
  short
  parse();

  /**
   * Get the tracelevel to use for CORBA communication.
   * @return the tracelevel or 0 if no tracelevel specified
   */
  unsigned int
  getTracelevel();

  /**
   * Get the Port to use for CORBA communication.
   * @return the port or 0 if no port specified
   */
  unsigned int
  getPort();

  /**
   * Get the dynamic start suffix, by default it's "START".
   * @return the dynmaic start suffix
   */
  char*
  getDynamicStartSuffix();

  /**
   * Get the dynamic stop suffix, by default it's "STOP".
   * @return the dynmaic stop suffix
   */
  char*
  getDynamicStopSuffix();

  /**
   * Get all dynamic system state tags.
   * If a tag name is "TAG1" then a pair of tags is expected : TAG1_START and
   * TAG1_STOP. START and STOP names are get from getDynamicStart-StopSuffix.
   * @return NULL if the file was not already parsed
   */
  tag_list_t*
  getDynamicTags();

  /**
   * Get all static system state tags.
   * @return NULL if the file was not already parsed
   */
  tag_list_t*
  getStaticTags();

  /**
   * Get all unique system state tags.
   * Only one information is keeped at a time. A new tag overwrite the
   * previous.
   * @return NULL if the file was not already parsed
   */
  tag_list_t*
  getUniqueTags();

  /**
   * Get all other tags (volatile tags).
   * @return NULL if the file was not already parsed
   */
  tag_list_t*
  getVolatileTags();

  /**
   * Get all the System state tags.
   * @return NULL if the file was not already parsed
   */
  tag_list_t*
  getStateTags();

  /**
   * Get all the possible tags.
   * @return NULL if the file was not already parsed
   */
  tag_list_t*
  getAllTags();

private:
  char*
  readLine(FILE* file);

  short
  parseTagSection(FILE* file, const char* sectionName, tag_list_t* taglist);

  void
  appendToList(tag_list_t* list, tag_list_t* appendlist);

  char* filename;
  bool alreadyParsed;

  unsigned int tracelevel;
  unsigned int port;
  char* startSuffix;
  char* stopSuffix;
  tag_list_t* dynamicTags;
  tag_list_t* staticTags;
  tag_list_t* uniqueTags;
  tag_list_t* volatileTags;
};

#endif

