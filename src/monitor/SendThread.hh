/****************************************************************************/
/* A thread that continously empties the tools outbuffers by sending them   */
/* to the corresponding toolMsgReceiver. Based on omni_thread and the orb.  */
/* Attention: causes memory leaks if the orb does not exist !!              */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ***************************************************************************/

#ifndef _SENDTHREAD_HH_
#define _SENDTHREAD_HH_

#include <omnithread.h>
#include "ToolList.hh"
#include "LogTool.hh"

class SendThread: public omni_thread {
public:
  /**
   * Creates a SendThread. The thread can be started with runThread().
   * Use stopThread() to stop and delete the thread.
   *
   * @param toolList. The toolList that the thread will work on.
   */
  explicit SendThread(ToolList* toolList);

  /**
   * Start the thread
   */
  void
  startThread();

  /**
   * Stops the thread. Waits for the thread to terminate properly.
   * Deletes the threadobject if the orb is running. A missing orb
   * results in memory leaks.
   */
  void
  stopThread();

protected:
  /**
   * Main function of thread
   * Contains main loop of thread, which has to end if
   * runSendThread is set to false
   */
  void*
  run_undetached(void* arg);

  /**
   * Destructor of function. Stops the thread if still running.
   * This function should not be called directly. The object is
   * implicitly deleted by the orb when join() is called. The
   * orb must exist to make this work.
   */
  ~SendThread();

private:
  /**
   * Shared variable that indicates if the thread is running or not.
   * stopThread will set this to false to stop the main loop of this
   * thread.
   */
  bool runSendThread;

  /**
   * Stores the toolList the thread uses
   */
  ToolList* toolList;
};

#endif
