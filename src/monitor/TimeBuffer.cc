/****************************************************************************/
/* Implementation for the TimeBuffer class                                  */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.2  2011/02/04 15:13:02  bdepardo
 * isOlder is now const
 *
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#include "TimeBuffer.hh"
#include <iostream>


TimeBuffer::TimeBuffer() {
  this->msgList = new MsgLinkedList();
  this->lastTime.sec = 0;
  this->lastTime.msec = 0;
}

TimeBuffer::~TimeBuffer() {
  delete this->msgList;
}

void
TimeBuffer::put(log_msg_t* msg) {
  msg->warning = false;
  // To get an read/write iterator for the messages list
  MsgLinkedList::Iterator* it = this->msgList->getIterator();
  // find the good place to insert the message (according to its timestamp)
  // Messages are order from older messages to newer messages (e.g. newer
  // messages are on the top of the list)
  it->resetToLast();
  while (it->hasCurrent()) {
    if (!isOlder(it->getCurrentRef()->time, msg->time)) {
      break;
    }
    it->previousRef();
  }
  if (it->hasCurrent()) {
    it->insertAfter(msg);
  } else {
    // must insert in the first position
    if (this->isOlder(msg->time, this->lastTime)) {
      msg->warning = true;
    }
    it->reset();
    it->insertBefore(msg);
  }
  delete it;
}

log_msg_t*
TimeBuffer::get(log_time_t minAge) {
  log_msg_t* msg = NULL;
  MsgLinkedList::Iterator* it = this->msgList->getIterator();
  if (it->hasCurrent()) {
    // check the age limit
    if (this->isOlder(minAge, it->getCurrentRef()->time)) {
      msg = it->removeAndGetCurrent();
      this->lastTime = msg->time;
    }
  }
  delete it;
  return msg;
}

// true if t2 older than t1
bool
TimeBuffer::isOlder(log_time_t t1, log_time_t t2) const {
  // important not to use <=
  return ((t2.sec < t1.sec)
          || ((t2.sec == t1.sec) && (t2.msec < t1.msec)));
}

