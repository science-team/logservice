/****************************************************************************/
/* Implementation of the ReadConfig class                                   */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.3  2011/02/04 15:13:40  bdepardo
 * Remove memleak.
 * Initialize members in constructor.
 *
 * Revision 1.2  2004/06/01 21:43:00  hdail
 * Tracking down seg fault in LogService:
 * - corrected mismatched malloc / delete[]
 * - corrected getLine(...) to return NULL if no more characters are available
 *   in the file (previous behavior tried to process the line)
 * - changed logic in all code in this file that calls getLine in order to
 *   properly check for NULL.  Also corrected apparent bug that last line of
 *   file may not have been processed under certain circumstances
 *
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#include "ReadConfig.hh"
#include <cstdio>
#include <cstdlib>
#include <cstring>

ReadConfig::ReadConfig(const char* configFilename, bool* success) {
  *success = false;
  this->filename = NULL;
  if (configFilename == NULL) {
    return;
  }
  FILE* file = fopen(configFilename, "r");
  if (file == NULL) {
    return;
  }
  fclose(file);
  this->filename = strdup(configFilename);
  this->alreadyParsed = false;
  this->tracelevel = 0;
  this->port = 0;
  this->startSuffix = NULL;
  this->stopSuffix = NULL;
  this->dynamicTags = new tag_list_t();
  this->staticTags = new tag_list_t();
  this->uniqueTags = new tag_list_t();
  this->volatileTags = new tag_list_t();
  *success = true;
}

ReadConfig::~ReadConfig() {
  if (this->filename != NULL) {
    free(this->filename);
  }
  delete this->dynamicTags;
  delete this->staticTags;
  delete this->uniqueTags;
  delete this->volatileTags;
}

char*
ReadConfig::readLine(FILE* file) {
  char* buff = NULL;
  int nextChar;

  // check for file content before trying to read line
  nextChar = fgetc(file);
  if (nextChar == EOF) {
    return NULL;
  }
  ungetc(nextChar, file);

  buff = new char[256];
  fgets(buff, 255, file);
  if (buff == NULL) {
    return NULL;
  }

  int len = strlen(buff);

  // remove \n
  if (buff[len - 1] == '\n') {
    len--;
    buff[len] = '\0';
  }

  // remove comments
  for (int i = 0; i < len; i++) {
    if (buff[i] == '#') {
      buff[i]='\0';
      len = i;
    }
  }

  // remove empty spaces at end of line
  for (int i = len-1; i >= 0; i--) {
    if (buff[i] == ' ') {
      buff[i] = '\0';
      len = i+1;
    } else {
      break;
    }
  }

  size_t sz = len + 1;
  char* ret = new char[sz];
  snprintf(ret, sz, "%s", buff);
  delete[] buff;
  return ret;
}

short
ReadConfig::parseTagSection(FILE* file, const char* sectionName,
                            tag_list_t* taglist) {
  rewind(file);
  int i = 0;
  int n = 0;
  bool found = false;
  char* s;
  // Find the section
  while (i == 0) {
    s = this->readLine(file);
    if (s == NULL) {
      i = 2;    // stop if end of file
    } else if (strcmp(s, sectionName) == 0) {
      i = 1;
    } else if (feof(file)) {
      i = 2;    // stop if end of file
    }
    delete[] s;
  }
  if (i == 2) {
    return LS_PARSE_SECTIONNOTFOUND;
  }
  // Parse the section
  i = 0;
  while (i == 0) {
    s = this->readLine(file);
    // stop if new section or end of file
    if ((s == NULL) || s[0] == '[') {
      i = 1;
      // avoid empty lines and commented lines
    } else if (strcmp(s, "") != 0) {
      n = taglist->length();
      found = false;
      for (CORBA::Long j = 0; j < n; j++) {
        if (strcmp(s, (*taglist)[j]) == 0) {
          found = true;
          break;
        }
      }
      if (!found) {
        // increase the length of the list
        taglist->length(n + 1);
        // add the tag
        (*taglist)[n] = CORBA::string_dup(s);
      }
    }
    delete[] s;
  }
  return LS_OK;
}

short
ReadConfig::parse() {
  if (this->alreadyParsed) {
    return LS_PARSE_ALREADYPARSED;
  }

  FILE* file = fopen(filename, "r");
  if (file == NULL) {
    return LS_PARSE_FILEERROR;
  }

  short i = 0;
  int ltracelevel = 0;
  int lport = 0;
  int lstart = 0;
  int lstop = 0;
  char* s;
  char* s2;

  // First find the General section
  while (i == 0) {
    s = this->readLine(file);
    if (s == NULL) {
      i = 2;  // stop if end of file
    } else if (strcmp(s, "[General]") == 0) {
      i = 1;
    } else if (feof(file)) {
      i = 2;  // stop if end of file
    }
    delete[] s;
  }
  if (i == 2) {
    return LS_PARSE_SECTIONNOTFOUND;
  }
  // Parse the General section
  i = 0;
  ltracelevel = strlen("Tracelevel=");
  lport = strlen("Port=");
  lstart = strlen("DynamicStartSuffix=");
  lstop = strlen("DynamicStopSuffix=");
  this->tracelevel = 0;
  this->port = 0;
  this->startSuffix = NULL;
  this->stopSuffix = NULL;
  while (i == 0) {
    s = this->readLine(file);
    if((s == NULL) || (s[0] == '[')){
      i = 1;  // stop if new section or end of file
    } else if (strncmp(s, "Tracelevel=", ltracelevel) == 0) {
      sscanf(s, "Tracelevel=%u", &(this->tracelevel));
    } else if (strncmp(s, "Port=", lport) == 0) {
      sscanf(s, "Port=%u", &(this->port));
    } else if (strncmp(s, "DynamicStartSuffix=", lstart) == 0) {
      s2 = new char[strlen(s) - lstart + 1];
      sscanf(s, "DynamicStartSuffix=%s", s2);
      this->startSuffix = strdup(s2);
      delete[] s2;
    } else if (strncmp(s, "DynamicStopSuffix=", lstop) == 0) {
      s2 = new char[strlen(s) - lstop + 1];
      sscanf(s, "DynamicStopSuffix=%s", s2);
      this->stopSuffix = strdup(s2);
      delete[] s2;
    } else if (feof(file)) {
      i = 1;  // stop if new section or end of file
    }
    delete[] s;
  }
  if (this->startSuffix == NULL) {
    this->startSuffix = new char[6];
    strncpy(this->startSuffix, "START", 6);
  }
  if (this->stopSuffix == NULL) {
    this->stopSuffix = new char[5];
    strncpy(this->stopSuffix, "STOP", 5);
  }


  // Find the DynamicTagList section
  i = this->parseTagSection(file, "[DynamicTagList]", this->dynamicTags);
  if (i != LS_OK) {
    return i;
  }

  // Find the StaticTagList section
  i = this->parseTagSection(file, "[StaticTagList]", this->staticTags);
  if (i != LS_OK) {
    return i;
  }

  // Find the UniqueTagList section
  i = this->parseTagSection(file, "[UniqueTagList]", this->uniqueTags);
  if (i != LS_OK) {
    return i;
  }

  // Find the VolatileTagList section
  i = this->parseTagSection(file, "[VolatileTagList]", this->volatileTags);
  if (i != LS_OK) {
    return i;
  }

  fclose(file);
  this->alreadyParsed = true;
  return LS_OK;
}

unsigned int
ReadConfig::getTracelevel() {
  unsigned int ret = 0;
  if (this->alreadyParsed) {
    ret = this->tracelevel;
  }
  return ret;
}

unsigned int
ReadConfig::getPort() {
  unsigned int ret = 0;
  if (this->alreadyParsed) {
    ret = this->port;
  }
  return ret;
}

char*
ReadConfig::getDynamicStartSuffix() {
  char* ret = NULL;
  if (this->alreadyParsed) {
    ret = strdup(this->startSuffix);
  }
  return ret;
}

char*
ReadConfig::getDynamicStopSuffix() {
  char* ret = NULL;
  if (this->alreadyParsed) {
    ret = strdup(this->stopSuffix);
  }
  return ret;
}

tag_list_t*
ReadConfig::getDynamicTags() {
  tag_list_t* ret = NULL;
  if (this->alreadyParsed) {
    ret = new tag_list_t(*(this->dynamicTags));
  }
  return ret;
}

tag_list_t*
ReadConfig::getStaticTags() {
  tag_list_t* ret = NULL;
  if (this->alreadyParsed) {
    ret = new tag_list_t(*(this->staticTags));
  }
  return ret;
}

tag_list_t*
ReadConfig::getUniqueTags() {
  tag_list_t* ret = NULL;
  if (this->alreadyParsed) {
    ret = new tag_list_t(*(this->uniqueTags));
  }
  return ret;
}

tag_list_t*
ReadConfig::getVolatileTags() {
  tag_list_t* ret = NULL;
  if (this->alreadyParsed) {
    ret = new tag_list_t(*(this->volatileTags));
  }
  return ret;
}

void
ReadConfig::appendToList(tag_list_t* list, tag_list_t* appendlist) {
  bool b = false;
  CORBA::ULong n1 = list->length();
  CORBA::ULong n2 = appendlist->length();
  for (CORBA::ULong i = 0; i < n2; i++) {
    b = false;
    for (CORBA::ULong j = 0; j < n1; j++) {
      if (strcmp((*appendlist)[i], (*list)[j]) == 0) {
        b = true;
        break;
      }
    }
    if (!b) {
      n1++;
      list->length(n1);
      (*list)[n1 - 1] = CORBA::string_dup((*appendlist)[i]);
    }
  }
}

tag_list_t*
ReadConfig::getStateTags() {
  tag_list_t* ret = NULL;
  if (this->alreadyParsed) {
    CORBA::ULong n = this->dynamicTags->length();
    ret = new tag_list_t();
    ret->length(2 * n);
    char* s1;
    char* s2;
    for (CORBA::ULong i = 0; i < n; i++) {
      s1 = (*this->dynamicTags)[i];
      size_t sz = strlen(s1) + strlen(this->startSuffix) + 2;
      s2 = new char[sz];
      snprintf(s2, sz, "%s_%s", s1, this->startSuffix);
      (*ret)[2 * i] = CORBA::string_dup(s2);
      delete[] s2;

      sz = strlen(s1) + strlen(this->stopSuffix) + 2;
      s2 = new char[sz];
      snprintf(s2, sz, "%s_%s", s1, this->stopSuffix);
      (*ret)[2 * i + 1] = CORBA::string_dup(s2);
      delete[] s2;
    }
    this->appendToList(ret, this->staticTags);
    this->appendToList(ret, this->uniqueTags);
  }
  return ret;
}

tag_list_t*
ReadConfig::getAllTags() {
  tag_list_t* ret = this->getStateTags();
  if (ret != NULL) {
    this->appendToList(ret, this->volatileTags);
  }
  return ret;
}

