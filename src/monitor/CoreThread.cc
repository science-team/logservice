/****************************************************************************/
/* Implementation for the Core thread                                       */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Georg Hoesch (hoesch@in.tum.de)                                     */
/*    - Cyrille Pontvieux (cyrille.pontvieux@edu.univ-fcomte.fr)            */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/
/* $Id$
 * $Log$
 * Revision 1.3  2010/12/08 11:37:54  kcoulomb
 * Refix the static library problem.
 * Renamed the monitor/Options to monitor/LogOptions due to conflict creating the static lib (2 files called Options)
 *
 * Revision 1.2  2004/01/13 15:07:15  ghoesch
 * speeded up message processing after poor results in tests
 *
 * Revision 1.1  2004/01/09 11:07:12  ghoesch
 * Restructured the whole LogService source tree.
 * Added autotools make process. Cleaned up code.
 * Removed some testers. Ready to release.
 *
 ****************************************************************************/

#include "CoreThread.hh"

#include <iostream>
#include "LogTypes.hh"
#include "LocalTime.hh"
#include "LogOptions.hh"


CoreThread::CoreThread(TimeBuffer* timeBuffer, StateManager* stateManager,
                       FilterManagerInterface* filterManager,
                       ToolList* toolList)
  : timeBuffer(timeBuffer), stateManager(stateManager),
  filterManager(filterManager), toolList(toolList),
  threadRunning(false) {
}

CoreThread::~CoreThread() {
  if (this->threadRunning) {
    this->threadRunning = false;
    join(NULL);
  }
}

void
CoreThread::startThread() {
  if (this->threadRunning) {
    return;
  }
  this->threadRunning = true;
  start_undetached();
}

void
CoreThread::stopThread() {
  if (!this->threadRunning) {
    return;
  }
  this->threadRunning = false;
  join(NULL);
}

void*
CoreThread::run_undetached(void* params) {
  log_time_t minAge;
  log_msg_t* msg = NULL;
  ToolList::ReadIterator* it = NULL;
  bool haveMsgs;
  while (this->threadRunning) {
    minAge = getLocalTime();
    minAge.sec -= LogOptions::CORETHREAD_MINAGE_TIME_SEC;
    minAge.msec -= LogOptions::CORETHREAD_MINAGE_TIME_MSEC;
    if (minAge.msec < 0) {
      minAge.sec--;
      minAge.msec += 1000;
    }
    haveMsgs = true;
    while (haveMsgs) {
      msg = this->timeBuffer->get(minAge);
      if (msg != NULL) {
        // we have a message
        if (this->stateManager->check(msg)) {
          // directly sent
          it = this->toolList->getReadIterator();
          while (it->hasCurrent()) {
            it->getCurrentRef()->outBuffer.push(msg);
            it->nextRef();
          }
          delete it;
        } else {
          // send to the FilterManager
          this->filterManager->sendMessageWithFilters(msg);
        }
        delete msg;
      } else {
        haveMsgs = false;
      }
    }

    sleep(LogOptions::CORETHREAD_SLEEP_TIME_SEC,
          LogOptions::CORETHREAD_SLEEP_TIME_NSEC);
  }
  return NULL;
}

