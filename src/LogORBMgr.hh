/****************************************************************************/
/* ORB manager v. 2.0 - CORBA management with DIET forwarders               */
/*                                                                          */
/*  Author(s):                                                              */
/*    - Gaël Le Mahec (gael.le.mahec@ens-lyon.fr)                           */
/*                                                                          */
/*  This file is part of DIET .                                             */
/*                                                                          */
/*  Copyright (C) 2000-2003 ENS Lyon, LIFC, INSA, INRIA and SysFera (2000)  */
/*                                                                          */
/*  - Frederic.Desprez@ens-lyon.fr (Project Manager)                        */
/*  - Eddy.Caron@ens-lyon.fr (Technical Manager)                            */
/*  - Tech@sysfera.com (Maintainer and Technical Support)                   */
/*                                                                          */
/*  This software is a computer program whose purpose is to provide an      */
/*  distributed logging services.                                           */
/*                                                                          */
/*                                                                          */
/*  This software is governed by the CeCILL license under French law and    */
/*  abiding by the rules of distribution of free software.  You can  use,   */
/*  modify and/ or redistribute the software under the terms of the CeCILL  */
/*  license as circulated by CEA, CNRS and INRIA at the following URL       */
/*  "http://www.cecill.info".                                               */
/*                                                                          */
/*  As a counterpart to the access to the source code and  rights to copy,  */
/*  modify and redistribute granted by the license, users are provided      */
/*  only with a limited warranty  and the software's author,  the holder    */
/*  of the economic rights,  and the successive licensors  have only        */
/*  limited liability.                                                      */
/*                                                                          */
/*  In this respect, the user's attention is drawn to the risks             */
/*  associated with loading,  using,  modifying and/or developing or        */
/*  reproducing the software by the user in light of its specific status    */
/*  of free software, that may mean  that it is complicated to              */
/*  manipulate, and  that  also therefore means  that it is reserved for    */
/*  developers and experienced professionals having in-depth computer       */
/*  knowledge. Users are therefore encouraged to load and test the          */
/*  software's suitability as regards their requirements in conditions      */
/*  enabling the security of their systems and/or data to be ensured and,   */
/*  more generally, to use and operate it in the same conditions as         */
/*  regards security.                                                       */
/*                                                                          */
/*  The fact that you are presently reading this means that you have had    */
/*  knowledge of the CeCILL license and that you accept its terms.          */
/*                                                                          */
/****************************************************************************/

#ifndef LOGORBMGR_HH
#define LOGORBMGR_HH

#include <list>
#include <map>
#include <string>
#include <sys/types.h>
#include <omniORB4/CORBA.h>
#include <omnithread.h>
#include "CorbaLogForwarder.hh"


#define LOGCOMPCTXT     "LogServiceC"
#define LOGTOOLCTXT     "LogServiceT"
#define LOGCOMPCONFCTXT "LogServiceC2"
#define LOGTOOLMSGCTXT  "LogServiceT2"
#define LOGFWRDCTXT     "LogForwarder"
#define DIETFWRD        "DietForwarder"


class LogORBMgr {
public:
  /* Constructors. */
  LogORBMgr(int argc, char* argv[]);

  explicit LogORBMgr(CORBA::ORB_ptr ORB);

  LogORBMgr(CORBA::ORB_ptr ORB, PortableServer::POA_var POA);

  /* Destructor. */
  ~LogORBMgr();

  /* Bind the object using its ctxt/name */
  void
  bind(const std::string& ctxt, const std::string& name,
       CORBA::Object_ptr object, const bool rebind = false) const;

  /* Bind an object using its IOR. */
  void
  bind(const std::string& ctxt, const std::string& name,
       const std::string& IOR, const bool rebind = false) const;

  /* Rebind objects. */
  void
  rebind(const std::string& ctxt, const std::string& name,
         CORBA::Object_ptr object) const;

  void
  rebind(const std::string& ctxt, const std::string& name,
         const std::string& IOR) const;

  /* Unbind an object. */
  void
  unbind(const std::string& ctxt, const std::string& name) const;

  /* Forwarders binding. */
  void
  fwdsBind(const std::string& ctxt, const std::string& name,
           const std::string& ior, const std::string& fwName = "") const;

  /* Forwarders unbinding. */
  void
  fwdsUnbind(const std::string& ctxt, const std::string& name,
             const std::string& fwName = "") const;

  /* Resolve an object using its IOR or ctxt/name. */
  CORBA::Object_ptr
  resolveObject(const std::string& IOR) const;

  CORBA::Object_ptr
  resolveObject(const std::string& ctxt, const std::string& name,
                const std::string& fwdName = "") const;

  /* Resolve objects without object caching or invoking forwarders. */
  CORBA::Object_ptr
  simpleResolve(const std::string& ctxt, const std::string& name) const;

  /* Get the list of the objects id binded in the omniNames server
     for a given context. */
  std::list<std::string>
  list(CosNaming::NamingContext_var& ctxt) const;

  std::list<std::string>
  list(const std::string& ctxtName) const;

  /* Get the list of declared CORBA contexts. */
  std::list<std::string>
  contextList() const;

  /* Return true if the object is local, false if it is reachable
     through a forwarder. */
  bool
  isLocal(const std::string& ctxt, const std::string& name) const;

  /* Return the name of the forwarder that manage the object. */
  std::string
  forwarderName(const std::string& ctxt, const std::string& name) const;

  template <typename CORBA_object, typename CORBA_ptr>
  CORBA_ptr
  resolve(const std::string& ctxt, const std::string& name,
          const std::string& fwdName = "") const {
    return CORBA_object::_duplicate(
      CORBA_object::_narrow(resolveObject(ctxt, name, fwdName)));
  }

  template <typename CORBA_object, typename CORBA_ptr>
  CORBA_ptr
  resolve(const std::string& IOR) const {
    return CORBA_object::_duplicate(CORBA_object::_narrow(resolveObject(IOR)));
  }

  /* Return the IOR of the passed object. */
  std::string
  getIOR(CORBA::Object_ptr object) const;

  std::string
  getIOR(const std::string& ctxt, const std::string& name) const;

  /* Activate an object. */
  void
  activate(PortableServer::ServantBase* object) const;

  /* Deactivate an object. */
  void
  deactivate(PortableServer::ServantBase* object) const;

  /* Wait for the request on activated objects. */
  void
  wait() const;

  void
  shutdown(bool waitForCompletion);

  static void
  init(int argc, char* argv[]);

  static LogORBMgr*
  getMgr();

  std::list<std::string>
  forwarderObjects(const std::string& fwdName, const std::string& ctxt) const;

  std::list<std::string>
  localObjects(const std::string& ctxt) const;

  /* IOR management functions. */
  static void
  makeIOR(const std::string& strIOR, IOP::IOR& ior);

  static void
  makeString(const IOP::IOR& ior, std::string& strIOR);

  static std::string
  getHost(IOP::IOR& ior);

  static std::string
  getHost(const std::string& strIOR);

  static unsigned int
  getPort(IOP::IOR& ior);

  static unsigned int
  getPort(const std::string& strIOR);

  static std::string
  getTypeID(IOP::IOR& ior);

  static std::string
  getTypeID(const std::string& strIOR);

  static std::string
  convertIOR(IOP::IOR& ior, const std::string& host,
             const unsigned int port);

  static std::string
  convertIOR(const std::string& ior, const std::string& host,
             const unsigned int port);

  /* Object cache management functions. */
  void
  resetCache() const;

  void
  removeObjectFromCache(const std::string& name) const;

  void
  removeObjectFromCache(const std::string& ctxt,
                        const std::string& name) const;
  void
  cleanCache() const;

  static void
  hexStringToBuffer(const char* ptr, const size_t size,
                    cdrMemoryStream& buffer);

private:
  /* The omniORB Object Request Broker for this manager. */
  CORBA::ORB_ptr ORB;
  /* The Portable Object Adaptor. */
  PortableServer::POA_var POA;

  /* Is the ORB down? */
  bool down;

  /* CORBA initialization. */
  void
  init(CORBA::ORB_ptr ORB);

  /* Object cache to avoid to contact OmniNames too many times. */
  mutable std::map<std::string, CORBA::Object_ptr> cache;
  /* Cache mutex. */
  mutable omni_mutex cacheMutex;

  /* The manager instance. */
  static LogORBMgr* theMgr;

  static void
  sigIntHandler(int sig);
#ifndef __cygwin__
  static omni_mutex waitLock;
#else
  static sem_t waitLock;
#endif
};


#endif
